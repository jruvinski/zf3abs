<?php

namespace ZF3Abs;

return [
    'view_manager' => [
        'doctype' => 'HTML5',
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'configuration' => [
            'orm_default' => require __DIR__ . '/doctrine.extensions.php',
        ]
    ],
];
