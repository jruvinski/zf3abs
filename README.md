# Zend 3 Abstract
 (projeto privado, chama muitas classes inacessíveis para esse módulo sozinho, então quem tiver interesse infelizmente não vai funcionar, e as classes dependentes não podemos fornecer)

## Instalação

#### Composer
```json
{
    "require": {
        "@reponame/zf3abs" : "*"
    }
}
```

#### config/modules.config.php
Adicione o módulo para carregamento no arquivo `config/modules.config.php`, antes dos seus módulos
```php
return [
    ...
    'ZF3Abs',
    ...
];
```

## Uso
Existem várias classes abstratas que podem ser referenciadas no seu código:
* `Controller/AbstractController` - Conta com um CRUD personalizado.
* `Entity/AbstractEntity` - Base das entities.
* `Form/AbstractForm` - Base padrão dos formulários, gera automaticamente conforme a configuração de cada campo.
* `Repository/AbstractRepository` - Base dos repositório das entities, conta com diversos métodos e helpers para o Abstract controler, incluindo as operações de crud e montagem da configuração de cada campo pra o form.
* `Service` - Algumas chamadas e serviço para a aplicação (envio de email, login, controle de acesso, navegação)
* `Util` - Classes úteis, para controle de banco, arquivo, email, seo, imagens, texto, etc...
* `ViewHelpers` - Algumas utilidades na view (controle de acesso, usuário atual, menu e breadcrumb)
