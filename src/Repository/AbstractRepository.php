<?php

namespace ZF3Abs\Repository;

use Doctrine\ORM\Query\Expr\Join;
use ZF3Abs\Util\DB;
use ZF3Abs\Util\Utils;

abstract class AbstractRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param \Doctrine\ORM\QueryBuilder $query
     * @return mixed
     */
    protected function executaBusca(\Doctrine\ORM\QueryBuilder $query)
    {
        return $query->getQuery()->execute();
    }

    /**select2
     * @param int $nid
     * @return mixed
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function buscaId($nid)
    {
        $query = $this
            ->createQueryBuilder('a1')
            ->select()
            ->andWhere("a1.{$this->_class->getSingleIdentifierFieldName()} = :pNid")
            ->setParameter('pNid', $nid);

        $resultado = $this->executaBusca($query);
        if (count($resultado)) {
            return $resultado[0];
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function buscaTudo()
    {
        $query = $this
            ->createQueryBuilder('a1')
            ->select();

        return $this->executaBusca($query);
    }

    /**
     * @param array $busca
     * @param array $campos
     * @return mixed
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function buscaPainel(Array $busca = [], $campos = [])
    {
        $query = $this->createQueryBuilder('a1');

        $joinCounter = 1;
        $select = ['a1'];
        $joins = [];

        foreach ($campos as $campo => $opc) {
            $exCampo = explode(':', $campo);
            if (count($exCampo) == 3 && preg_match('/(idiomas:)/', $campo) && isset($opc['joinColumns']) && count($opc['joinColumns'])) {

                if (!isset($joins[$exCampo[0]])) {
                    $joinCounter++;
                    $joins[$exCampo[0]] = "a{$joinCounter}";
                }
                $srcCampo = reset($opc['joinColumnFieldNames']);
                $praCampo = end($opc['joinColumnFieldNames']);
                $query->leftJoin($opc['opcoes']['targetEntity'], $joins[$exCampo[0]], Join::WITH, "{$joins[$exCampo[0]]}.{$srcCampo} = a1.{$praCampo}");
                $query->leftJoin("{$joins[$exCampo[0]]}.idioma", 'ai2', Join::WITH, 'ai2.padrao = 1');
            }

            if ($campo == $opc['primary']) {
                $query->groupBy("a1.{$campo}");
            }

        }

        foreach ($busca as $k => $opcoes) {

            $consulta = '';
            if (preg_match('(\\:)', $opcoes[1])) {
                $join = explode(':', $opcoes[1]);

                $colInfo = $this->infoColunas([$opcoes[1]])[$opcoes[1]];
                if (!isset($joins['idioma'])) {
                    $joins['idioma'] = 'ai';
                    $query->leftJoin("a1.idioma", 'ai');
                    $query->andWhere("ai.padrao = '1'");
                }

                if (!isset($joins[$join[0]])) {
                    $joinCounter++;
                    $joins[$join[0]] = "a{$joinCounter}";
                    $query->leftJoin("a1.{$join[0]}", $joins[$join[0]]);
                }

                if ($opcoes[0] == 'orderBy') {
                    $query->{$opcoes[0]}("{$joins[$join[0]]}.{$join[0]}", $opcoes[2]);
                } else {
                    $consulta = "{$joins[$join[0]]}.{$join[1]} {$opcoes[2]}" . (isset($opcoes[3]) ? " '{$opcoes[3]}'" : '');
                }

            } elseif (isset($opcoes[4]) && preg_match('(\\|)', $opcoes[4])) {
                $join = explode('|', $opcoes[1]);
                $joinAlternativo = explode('|', $opcoes[4]);

                if (!isset($joins[$join[0]])) {
                    $joinCounter++;
                    $joins[$join[0]] = "a{$joinCounter}";
                }
                $query->leftJoin("a1.{$join[0]}", $joins[$join[0]]);

                if (!isset($joins[$join[1]]) && count($join) == '3') {
                    $joinCounter++;
                    $joins[$join[1]] = "a{$joinCounter}";
                }
                $query->leftJoin("{$joins[$join[0]]}.{$join[1]}", $joins[$join[1]]);

                if (!isset($joins[$joinAlternativo[0]])) {
                    $joinCounter++;
                    $joins[$joinAlternativo[0]] = "a{$joinCounter}";
                }
                $query->leftJoin("a1.{$joinAlternativo[0]}", $joins[$joinAlternativo[0]]);

                if (!isset($joins[$joinAlternativo[0] . $joinAlternativo[1]]) && count($join) == '3') {
                    $joinCounter++;
                    $joins[$joinAlternativo[0] . $joinAlternativo[1]] = "a{$joinCounter}";
                }
                $query->leftJoin("{$joins[$joinAlternativo[0]]}.{$joinAlternativo[1]}", $joins[$joinAlternativo[0] . $joinAlternativo[1]]);

                if ($opcoes[0] == 'orderBy') {

                    $query->{$opcoes[0]}("{$joins[$join[0]]}.{$opcoes[1]}", $opcoes[2]);
                    $query->{$opcoes[0]}("{$joins[$joinAlternativo[0]]}.{$opcoes[1]}", $opcoes[2]);
                } elseif (count($join) == 3) {
                    $consulta = "({$joins[$join[1]]}.{$join[2]} {$opcoes[2]}" . (isset($opcoes[3]) ? " '{$opcoes[3]}'" : '');
                    if (count($joinAlternativo) == 3) {
                        $consulta .= (isset($opcoes[3]) ? ' OR ' : ' AND ') . "{$joins[$joinAlternativo[0].$joinAlternativo[1]]}.{$joinAlternativo[2]} {$opcoes[2]}" . (isset($opcoes[3]) ? " '{$opcoes[3]}'" : '') . ')';
                    } else {
                        $consulta .= (isset($opcoes[3]) ? ' OR ' : ' AND ') . "{$joins[$joinAlternativo[0]]}.{$joinAlternativo[1]} {$opcoes[2]}" . (isset($opcoes[3]) ? " '{$opcoes[3]}'" : '') . ')';
                    }
                } else {
                    $consulta = "({$joins[$join[0]]}.{$join[1]} {$opcoes[2]}" . (isset($opcoes[3]) ? " '{$opcoes[3]}'" : '');
                    if (count($joinAlternativo) == 3) {
                        $consulta .= (isset($opcoes[3]) ? ' OR ' : ' AND ') . "{$joins[$joinAlternativo[0].$joinAlternativo[1]]}.{$joinAlternativo[2]} {$opcoes[2]}" . (isset($opcoes[3]) ? " '{$opcoes[3]}'" : '') . ')';
                    } else {
                        $consulta .= (isset($opcoes[3]) ? ' OR ' : ' AND ') . "{$joins[$joinAlternativo[0]]}.{$joinAlternativo[1]} {$opcoes[2]}" . (isset($opcoes[3]) ? " '{$opcoes[3]}'" : '') . ')';
                    }
                }

            } elseif (preg_match('(\\|)', $opcoes[1])) {
                $join = explode('|', $opcoes[1]);

                if (!isset($joins[$join[0]])) {
                    $joinCounter++;
                    $joins[$join[0]] = "a{$joinCounter}";
                    $query->leftJoin("a1.{$join[0]}", $joins[$join[0]]);
                }

                if (!isset($joins[$join[1]]) && count($join) == '3') {
                    $joinCounter++;
                    $joins[$join[1]] = "a{$joinCounter}";
                    $query->leftJoin("{$joins[$join[0]]}.{$join[1]}", $joins[$join[1]]);
                }

                if ($opcoes[0] == 'orderBy') {

                    $query->{$opcoes[0]}("{$joins[$join[0]]}.{$opcoes[1]}", $opcoes[2]);
                } elseif (count($join) == 3) {
                    $consulta = "{$joins[$join[1]]}.{$join[2]} {$opcoes[2]}" . (isset($opcoes[3]) ? ($opcoes[2] == 'IN' ? " {$opcoes[3]}" : " '{$opcoes[3]}'") : '');
                } else {
                    $consulta = "{$joins[$join[0]]}.{$join[1]} {$opcoes[2]}" . (isset($opcoes[3]) ? ($opcoes[2] == 'IN' ? " {$opcoes[3]}" : " '{$opcoes[3]}'") : '');
                }
            } else {

                if ($opcoes[0] == 'orderBy') {

                    $query->{$opcoes[0]}("a1.{$opcoes[1]}", $opcoes[2]);
                } else {
                    $consulta = "a1.{$opcoes[1]} {$opcoes[2]}" . (isset($opcoes[3]) ? " '{$opcoes[3]}'" : '');
                }
            }
            if ($consulta !== '') {
                $query->{$opcoes[0]}($consulta);
            }
        }

        $query->select($select);

        return $this->executaBusca($query);
    }

    /**
     * @param array $where
     * @return mixed
     */
    public function select2($where = [])
    {
        $query = $this->createQueryBuilder('a1');
        $query->select();

        foreach ($where['join'] as $join => $alias) {
            $query->leftJoin((count($alias) == 2 ? $alias[1] : 'a1') . ".{$join}", $alias[0]);
        }

        if (count($where['and'])) {
            $query->andWhere(implode(' AND ', $where['and']));
        }

        if (count($where['or'])) {
            $query->andWhere(implode(' OR ', $where['or']));
        }

        return $this->executaBusca($query);
    }

    /**
     * @param array $where
     * @return mixed
     */
    public function getLista($where = [])
    {
        $query = $this->createQueryBuilder('a1');
        $query->select();

        if ($where) {
            $query->andWhere("a1.{$where}");
        }

        return $this->executaBusca($query);
    }

    /**
     * @param $sort
     * @param string $order
     * @param bool $where
     * @return mixed
     */
    public function getListaOrdenada($sort, $order = 'ASC', $where = false)
    {
        $query = $this->createQueryBuilder('a1');
        $query
            ->select()
            ->andWhere("a1.status = '1'")
            ->orderBy("a1.{$sort}", $order);

        if ($where) {
            $query->andWhere("a1.{$where}");
        }

        return $this->executaBusca($query);
    }

    /**
     * @param $sort
     * @param string $order
     * @param bool $where
     * @return mixed
     */
    public function getListaOrdenadaIdioma($sort, $order = 'ASC', $where = false)
    {
        $query = $this->createQueryBuilder('a1');
        $query
            ->select()
            ->orderBy("a1.{$sort}", $order);

        if ($where) {
            $query->andWhere($where);
        }

        return $this->executaBusca($query);
    }

    /**
     * @param $dados
     * @param $colunas
     * @param $entity
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function salvaRepo($dados, $colunas, $entity)
    {
        $primary = reset($colunas)['primary'];

        $entityLoaded = $this->_em->getRepository($entity);

        if (!$row = $entityLoaded->find($dados[$primary])) {

            $row = new $entity();
        }

        foreach ($colunas as $col => $opt) {

            $val = [];
            if (!array_key_exists($col, $dados)) {
                if ($opt['f-type'] != 'many')
                    continue;
            } else {
                $val = $dados[$col];
            }

            if ($col == $primary) {

                continue;
            } elseif ($opt['f-type'] == 'many') {

                DB::remove($row, $col);
                $repoTarget = $this->_em->getRepository($opt['target']);
                foreach ($val as $item) {
                    DB::add($row, $col, $repoTarget->find($item));
                }
            } elseif ($opt['f-type'] == 'idioma') {

                $colEx = explode(':', $col);
                if (count($colEx) > 2 && $colEx[1] == 'idiomas') {
                    $repoTarget = $this->_em->getRepository($opt['targetEntity']);
                    $obj = $dados[$col] == null ? null : $repoTarget->find($dados[$col]);
                    DB::set($row, $col, $obj);
                }
            } elseif ($opt['f-type'] == 'foreign') {

                $nRepo = $this->_em->getRepository($opt['target']);
                $obj = $val ? $nRepo->find($val) : $val;
                DB::set($row, $col, $obj);
            } elseif ($opt['f-type'] == 'normal') {

                if (preg_match('/(date|datetime|timestamp)/', $opt['type']))
                    $val = Utils::DateTime($val);

                DB::set($row, $col, $val);
                if ($col == 'nome' && in_array('rewrite', $this->getClassMetadata()->getFieldNames(), true)) {
                    DB::set($row, 'rewrite', DB::makeRewrite($this, $row->getId(), $val));
                }
            }
        }

        $this->_em->persist($row);

        if (isset($dados['idiomas'])) {

            $val = $dados['idiomas'];
            $colOpt = $colunas['idiomas:' . @reset(array_keys(reset($val)))];
            foreach ($val as $id_idioma => $cols_idioma) {

                $rowIdiomaRep = $this->_em->getRepository($colOpt['targetEntity']);
                if (!$rowIdioma = $rowIdiomaRep->findOneBy(['idioma' => $id_idioma, $colOpt['mappedBy'] => $row->getId()])) {
                    $rowIdioma = new $colOpt['targetEntity']();
                }
                foreach ($cols_idioma as $nome_col_idioma => $val_idioma) {
                    DB::set($rowIdioma, $nome_col_idioma, $val_idioma);

                    if ($nome_col_idioma == 'nome' && in_array('rewrite', $rowIdiomaRep->getClassMetadata()->getFieldNames(), true)) {
                        DB::set($rowIdioma, 'rewrite', DB::makeRewrite($rowIdiomaRep, $rowIdioma->getId(), $val_idioma));
                    }
                }
                DB::set($rowIdioma, 'idioma', $this->_em->getRepository(Idioma::class)->find($id_idioma));
                DB::set($rowIdioma, $colOpt['mappedBy'], $row);

                $this->_em->persist($rowIdioma);
                $row->addIdioma($rowIdioma);
            }
        }

        $this->_em->flush();

        return $row;
    }

    /**
     * Marca o registro como ativo
     *
     * @param $nid
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function ativar($nid)
    {
        $row = $this->find(['id' => $nid]);
        $row->setStatus(\ZF3Abs\Entity\AbstractEntity::STATUS_ATIVO);

        $this->_em->persist($row);
        $this->_em->flush();
    }

    /**
     * Marca o registro como inativo
     *
     * @param $nid
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function inativar($nid)
    {
        $row = $this->find(['id' => $nid]);
        $row->setStatus(\ZF3Abs\Entity\AbstractEntity::STATUS_INATIVO);

        $this->_em->persist($row);
        $this->_em->flush();
    }

    /**
     * Marca o registro como pendente
     *
     * @param $nid
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function pendente($nid)
    {
        $row = $this->find(['id' => $nid]);
        $row->setStatus(\ZF3Abs\Entity\AbstractEntity::STATUS_PENDENTE);

        $this->_em->persist($row);
        $this->_em->flush();
    }

    /**
     * Marca o registro como excluido
     *
     * @param $nid
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function excluir($nid)
    {
        $row = $this->find(['id' => $nid]);
        $row->setStatus(\ZF3Abs\Entity\AbstractEntity::STATUS_APAGADO);

        $this->_em->persist($row);
        $this->_em->flush();
    }

    /**
     * @param array $para
     * @param array $superCol
     * @return array
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function infoColunas($para = [], $opcCol = [], $superCol = [])
    {
        $colunas = [];
        $metaData = $this->_em->getClassMetadata($this->_entityName);
        foreach ($para as $item) {
            $identifier = $metaData->getSingleIdentifierFieldName();
            $defautRepositoryLoad = new $this->_entityName();

            if (isset($metaData->getAssociationMappings()[$item])) {

                $manyMany = $metaData->getAssociationMapping($item);
                $parentEntity = false;

                $manyManyRepo = $this->_em->getClassMetadata($manyMany['targetEntity']);
                $associacoes = $manyManyRepo->getAssociationMappings();
                if (count($associacoes)) {
                    $resetEntity = reset($associacoes);
                    if ($resetEntity['isOwningSide'])
                        $parentEntity = $resetEntity['targetEntity'];
                }

                $colunas[$item]['label'] = isset($defautRepositoryLoad->_colNome[$item]) ? $defautRepositoryLoad->_colNome[$item] : $item;
                if(isset($opcCol[$item]['semFiltro'])) {
                    $colunas[$item]['disableFilter'] = true;
                } else {

                    $multi = [];
                    foreach ($this->_em->getRepository($manyMany['targetEntity'])->getLista() as $v) {
                        $multi[$v->getId()] = $v->getNome();
                    }
                    $colunas[$item]['specialFilter'] = $multi;
                }

                $colunas[$item]['type'] = 'string';
                $colunas[$item]['desc'] = isset($defautRepositoryLoad->_colDesc[$item]) ? $defautRepositoryLoad->_colDesc[$item] : '';
                $colunas[$item]['target'] = $manyMany['targetEntity'];
                $colunas[$item]['parent'] = isset($superCol[$item]['busca_parent']) && $superCol[$item]['busca_parent'] ? $parentEntity : false;
                $colunas[$item]['primary'] = $identifier;
                $colunas[$item]['f-type'] = 'many';
//                echo '<pre>';var_dump($colunas);exit;

            } elseif (strstr($item, '|')) {
                // Colunas de chave estrangeira
                $itemExploded = explode('|', $item);

                $sub = $metaData->getAssociationMapping($itemExploded[0]);
                $targetEntity = $sub['targetEntity'];
                $targetParentEntity = false;
                if (count($itemExploded) > 2) {
                    foreach ($itemExploded as $k => $itEx) {
                        if ($k == 0 || $k == (count($itemExploded) - 1)) continue;

                        $metaDataSub = $this->_em->getClassMetadata($sub['targetEntity']);
                        $sub = $metaDataSub->getAssociationMapping($itemExploded[$k]);
                    }
                    $targetParentEntity = $targetEntity;
                    $targetEntity = $sub['targetEntity'];

                }
                $metaDataSub = $this->_em->getClassMetadata($sub['targetEntity']);
                $identifier = $metaDataSub->getSingleIdentifierFieldName();

                $parentEntity = false;
                $metaDataSubAssociationMappings = $metaDataSub->getAssociationMappings();
                foreach ($metaDataSubAssociationMappings as $nomeCol => $maps) {
                    if (isset($maps['joinTableColumns']) && count($maps['joinTableColumns']) > 1) continue;
                    if ($maps['sourceEntity'] == $targetEntity && !$maps['mappedBy']) {

                        $parentEntity = [
                            'targetEntity' => $maps['targetEntity'],
                            'fieldName' => $maps['fieldName'],
                        ];
                    }
                }

                $colunas[$item] = $metaDataSub->getFieldMapping($itemExploded[count($itemExploded) - 1]);
                $colunas[$item]['label'] = isset($defautRepositoryLoad->_colNome[$itemExploded[0]]) ? $defautRepositoryLoad->_colNome[$itemExploded[0]] : '';
                $colunas[$item]['desc'] = isset($defautRepositoryLoad->_colDesc[$itemExploded[0]]) ? $defautRepositoryLoad->_colDesc[$itemExploded[0]] : '';
                $colunas[$item]['target'] = $targetEntity;
                $colunas[$item]['target-parent'] = $targetParentEntity;
                $colunas[$item]['parent'] = isset($superCol[$item]['busca_parent']) && $superCol[$item]['busca_parent'] ? $parentEntity : false;
                $colunas[$item]['primary'] = $identifier;
                $colunas[$item]['f-type'] = 'foreign';

                foreach ($sub['joinColumns'] as $jc) {
                    if ($jc['name'] == Utils::uppercaseDash($itemExploded[0], '_')) {
                        $colunas[$item]['nullable'] = $jc['nullable'];
                    }
                }

            } elseif (strstr($item, ':')) {
                // Colunas com idioma
                $itemExploded = explode(':', $item);

                $thisEntity = $this->getClassMetadata()->getName();
                $thisEntity = new $thisEntity();

                $colunas[$item] = $this->recursiveCampo($item);
                $nomes = array_merge($thisEntity->_colNome ?? [], $colunas[$item]['colNome']);
                $descs = array_merge($thisEntity->_colDesc ?? [], $colunas[$item]['colDesc']);

                $colunas[$item]['label'] = isset($nomes[$itemExploded[1]]) ? $nomes[$itemExploded[1]] : (isset($nomes[$itemExploded[0]]) ? $nomes[$itemExploded[0]] : '');
                $colunas[$item]['desc'] = isset($descs[$itemExploded[1]]) ? $descs[$itemExploded[1]] : (isset($descs[$itemExploded[0]]) ? $descs[$itemExploded[0]] : '');
                $colunas[$item]['f-type'] = 'idioma';

            } else {

                // Colunas comuns da tabela
                $colunas[$item] = $metaData->getFieldMapping($item);
                $colunas[$item]['label'] = isset($defautRepositoryLoad->_colNome[$item]) ? $defautRepositoryLoad->_colNome[$item] : '';
                $colunas[$item]['desc'] = isset($defautRepositoryLoad->_colDesc[$item]) ? $defautRepositoryLoad->_colDesc[$item] : '';
                $colunas[$item]['primary'] = $identifier;
                $colunas[$item]['f-type'] = 'normal';
            }

            $colunas[$item]['forcar'] = isset($superCol[$item]['forcar']) && $superCol[$item]['forcar'] ? $superCol[$item]['forcar'] : false;
            $colunas[$item]['mostra_parent_vazio'] = isset($superCol[$item]['mostra_parent_vazio']) && $superCol[$item]['mostra_parent_vazio'] ? $superCol[$item]['mostra_parent_vazio'] : false;
        }

        return $colunas;
    }

    /**
     * @param string $campo
     * @param bool $meta
     * @return array
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function recursiveCampo($campo, $meta = false)
    {
        $item = explode(':', $campo);
        $campoItem = [];
        if (count($item)) {
            $a1 = $item[0];
            unset($item[0]);

            $metaGeral = $this->getClassMetadata();
            if ($meta) {
                $metaGeral = $this->getEntityManager()->getRepository($meta)->getClassMetadata();
            }
            if (isset($metaGeral->getAssociationMappings()[$a1])) {
                $campoItem = $metaGeral->getAssociationMappings()[$a1];
            } else {
                $campoItem = $metaGeral->getFieldMapping($a1);
            }

            $item = implode(':', $item);
            if (isset($campoItem['targetEntity'])) {
                $opcoes = $this->recursiveCampo($item, $campoItem['targetEntity']);
                if (count($opcoes)) {
                    $campoItem['type'] = $opcoes['type'];
                    $campoItem['nullable'] = $opcoes['nullable'];
                    $campoItem['precision'] = $opcoes['scale'];
                    $campoItem['columnName'] = $opcoes['columnName'];
                    $campoItem['primary'] = $opcoes['columnName'];
                    $campoItem['opcoes'] = $opcoes;

                    $entity = new $campoItem['targetEntity']();
                    $campoItem['colNome'] = $entity->_colNome ?? [];
                    $campoItem['colDesc'] = $entity->_colDesc ?? [];
                }
            }
        }

        return $campoItem;
    }

}
