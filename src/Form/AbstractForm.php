<?php

namespace ZF3Abs\Form;

use Doctrine\ORM\EntityManager;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use ZF3Abs\Util\DB;
use ZF3Abs\Util\Utils;

/**
 * This form is used to collect user's login, password and 'Remember Me' flag.
 */
class AbstractForm extends Form
{
    /**
     * @var EntityManager
     */
    private $_entityManager;

    /**
     * @var string
     */
    private $_defaultEntity;

    /**
     * @var \ZF3Abs\Entity\AbstractEntity
     */
    private $_defaultEntityObject;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository
     */
    private $_defaultRepository;

    /**
     * @var array
     */
    private $_colunas;

    /**
     * @var array
     */
    private $_opcoes;

    /**
     * @var bool
     */
    private $_isAjax;

    private $inputFilter;

    /**
     * AbstractForm constructor.
     * @param EntityManager $em
     * @param string $entity
     * @param array $colunas
     * @param array $opcoes
     * @param bool $is_ajax
     */
    public function __construct(EntityManager $em, $entity, $colunas, $opcoes = [], $is_ajax = false)
    {
        $this->_entityManager = $em;
        $this->_defaultEntity = $entity;
        $this->_defaultEntityObject = new $entity();
        $this->_defaultRepository = $this->_entityManager->getRepository($entity);
        $this->_colunas = $colunas;
        $this->_isAjax = $is_ajax;
        $this->_opcoes = isset($opcoes['opcoes']) ? $opcoes['opcoes'] : [];

        $formName = Utils::urlAmigavel(@end(explode('\\', $entity)));
        // Define form name
        parent::__construct("{$formName}-form");

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('role', 'form');
        $this->setAttribute('class', 'form-horizontal');

        $this->inputFilter = new InputFilter();
        $this->setInputFilter($this->inputFilter);

        $this->addElements(isset($opcoes['imagem']) ? $opcoes['imagem'] : 0, isset($opcoes['arquivo']) ? $opcoes['arquivo'] : 0);
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements($imagens, $arquivos)
    {
        $tplAjudaColuna = ' <i class="fa fa-question-circle btn btn-flat btn-info" style="font-size: 14px;padding: 10px 4px 10px 2px;margin: 0;" title="" data-toggle="popover" data-placement="bottom" data-original-title="{{TITULO}}" data-content="{{CONTEUDO}}"></i>';
        foreach ($this->_colunas as $coluna => $opcoes) {

            if ($opcoes['f-type'] == 'many') {

                $target = $this->_entityManager->getRepository($opcoes['target']);
                $targetCol = @end(explode('\\', $opcoes['target']));

                if (isset($this->_opcoes[$coluna]) && in_array('many-select2', $this->_opcoes[$coluna], true)) {

                    $dados = [
                        'type' => 'select',
                        'name' => "{$coluna}",
                        'options' => [
                            'label' => $this->spanAjudaPopover($opcoes['label'], $opcoes['desc']),
                            'label_options' => [
                                'disable_html_escape' => true
                            ],
                            'label_attributes' => [
                                'class' => 'control-label col-md-3'
                            ],
                            'value_options' => [],
                            'disable_inarray_validator' => true,
                        ],
                        'attributes' => [
                            'class' => 'form-control select2memulti',
                            'multiple' => 'multiple',
                            'data-select2-entity' => $targetCol,
                            'data-select2-fields' => isset($this->_opcoes[$coluna]['campos']) ? $this->_opcoes[$coluna]['campos'] : 'nome',
                            'data-select2-maxselection' => isset($this->_opcoes[$coluna]['maxselection']) ? $this->_opcoes[$coluna]['maxselection'] : '0',
                            'data-select2-filter' => isset($this->_opcoes[$coluna]['filtro']) ? $this->_opcoes[$coluna]['filtro'] : '',
                        ]
                    ];

                    $this->add($dados);
                    $this->addInputFilter($coluna, false);

                    continue;
                }

                if ($opcoes['parent']) {

                    $parentCol = @strtolower(end(explode('\\', $opcoes['parent'])));
                    $tst = [];
                    $parentMesmaTabela = $targetCol == $parentCol;
                    $parent = $this->_entityManager->getRepository($opcoes['parent']);

                    foreach ($parent->getListaOrdenada('nome', 'ASC') as $parent_row) {
                        $parentName = \ZF3Abs\Util\Utils::urlAmigavel("{$parent_row->getNome()}-{$parent_row->getId()}");

                        $tst[$parentMesmaTabela ? $parent_row->getId() : "p_{$parent_row->getId()}"] = [
                            'value' => $parentMesmaTabela ? $parent_row->getId() : "p",
                            'label' => $parent_row->getNome(),
                            'label_attributes' => ['class' => "label-control col-xs-12", 'for' => "{$parentName}-todos" . ($parentMesmaTabela ? "_igual" : "")],
                        ];
                        try {
                            $targets = $target->getListaOrdenada('nome', 'ASC', "{$parentCol} = {$parent_row->getId()}");
                        } catch (\Exception $e) {
                            echo '<pre>';
                            var_dump($parentCol, $opcoes, "{$parentCol} = {$parent_row->getId()}");
                            echo "<br><br>{$e->getMessage()}";
                            echo "<br><br>{$e->getTraceAsString()}";
                            exit;
                        }

                        if (!count($targets)) {
                            unset($tst[$parentMesmaTabela ? $parent_row->getId() : "p_{$parent_row->getId()}"]);
                            continue;
                        }

                        foreach ($targets as $i => $target_row) {
                            $tst[$target_row->getId()] = [
                                'value' => $target_row->getId(),
                                'label' => $target_row->getNome(),
                                'label_attributes' => ['class' => "control-label col-xs-12 col-sm-6 col-md-4 col-lg-3"],
                                'attributes' => ['class' => 'minimal', 'data-target' => $parentName]
                            ];
                        }
                    }

                    if (!count($tst))
                        continue;

                    $this->add([
                        'type' => 'multicheckbox',
                        'name' => $coluna,
                        'options' => [
                            'label' => $opcoes['label'],
                            'label_options' => [
                                'disable_html_escape' => true
                            ],
                            'label_attributes' => [
                                'class' => 'control-label col-xs-12 col-sm-6 col-md-4 col-lg-3'
                            ],
                            'value_options' => $tst
                        ],
                        'attributes' => [
                            'class' => 'minimal',
                        ]
                    ]);
                    $this->addInputFilter($coluna, false);
                } else {

//                    $this->add($this->addCheckInput("{$targetCol}-todos", $opcoes['label'], false, '', 0));

                    $targets = $target->getListaOrdenada('nome', 'ASC');
                    foreach ($targets as $i => $target_row) {
                        $tst["{$targetCol}[{$target_row->getId()}]"] = [
                            'value' => $target_row->getId(),
                            'label' => $target_row->getNome(),
                            'label_attributes' => ['class' => "control-label col-xs-12 col-sm-6 col-md-4 col-lg-3"],
                            'attributes' => ['class' => 'minimal', 'data-target' => '']
                        ];
//                        $this->add($this->addCheckInput("{$targetCol}[{$target_row->getId()}]", $target_row->getNome(), ($i == 0 ? 'p' : (($i + 1) == count($targets) ? 'u' : 'm')), 'col-xs-12 col-sm-6 col-md-4 col-lg-3', $targetCol));
                    }
                    $this->add([
                        'type' => 'multicheckbox',
                        'name' => $coluna,
                        'options' => [
                            'label' => $opcoes['label'],
                            'label_options' => [
                                'disable_html_escape' => true
                            ],
                            'label_attributes' => [
                                'class' => 'control-label col-xs-12 col-sm-6 col-md-4 col-lg-3'
                            ],
                            'value_options' => $tst
                        ],
                        'attributes' => [
                            'class' => 'minimal',
                        ]
                    ]);
                }
            } elseif ($opcoes['f-type'] == 'foreign') {

                $multi = [];
                if (!(isset($this->_opcoes[$coluna]) && in_array('one-select2', $this->_opcoes[$coluna], true))) {
                    $multi = $this->getMultiOptions($opcoes, $coluna);
                }

                $targetCol = @end(explode('\\', $opcoes['target']));

                $curType = $this->getType($opcoes, $coluna);
                $dados = [
                    'type' => $curType,
                    'name' => $coluna,
                    'options' => [
                        'label' => $this->spanAjudaPopover($opcoes['label'], $opcoes['desc']),
                        'label_options' => [
                            'disable_html_escape' => true
                        ],
                        'label_attributes' => [
                            'class' => 'control-label col-md-3'
                        ],
                        'value_options' => $multi,
                        'help_attrs' => $this->linkAjaxTarget($opcoes, $coluna),
                        'disable_inarray_validator' => true,
                    ],
                    'attributes' => [
                        'class' => 'form-control select2me',
                        'value' => isset($opcoes['options']['default']) ? $opcoes['options']['default'] : ''
                    ]
                ];

                if (!$opcoes['nullable']) {
                    $dados['attributes']['required'] = 'required';
                }
                if ($dados['type'] == 'text' && isset($opcoes['length']) && $opcoes['length'] > 0) {
                    $dados['attributes']['maxlength'] = $opcoes['length'];
                }
                if (isset($opcoes['options']['default'])) {
                    $dados['value'] = $opcoes['options']['default'];
                }
                if (isset($this->_opcoes[$coluna]) && in_array('one-select2', $this->_opcoes[$coluna], true)) {
                    $dados['attributes']['class'] = 'form-control select2meajax';
                    $dados['attributes']['data-select2-entity'] = $targetCol;
                    $dados['attributes']['data-select2-fields'] = isset($this->_opcoes[$coluna]['campos']) ? $this->_opcoes[$coluna]['campos'] : $opcoes['fieldName'];
                    $dados['attributes']['data-select2-filter'] = isset($this->_opcoes[$coluna]['filtro']) ? $this->_opcoes[$coluna]['filtro'] : '';
                }

                $this->add($dados);
                $this->addInputFilter($coluna, !$opcoes['nullable']);
            } elseif ($opcoes['f-type'] == 'idioma') {

                $ajuda = strlen($opcoes['desc']) ?
                    str_replace(['{{TITULO}}', '{{CONTEUDO}}'], [\ZF3Abs\Util\Utils::tr($opcoes['label']), \ZF3Abs\Util\Utils::tr($opcoes['desc'])], $tplAjudaColuna) :
                    '';
                $curType = $this->getType($opcoes, $coluna);

                if ($curType != 'select') {
                    $colsIdiomas = explode(':', $coluna);
                    foreach (\ZF3Abs\Util\DB::getIdiomas() as $idioma) {
                        $colunaArray = "{$colsIdiomas[0]}[{$idioma->getId()}][{$colsIdiomas[1]}]";
                        $dados = [
                            'type' => $curType,
                            'name' => $colunaArray,
                            'options' => [
                                'label' => \ZF3Abs\Util\Utils::tr($opcoes['label']) . $ajuda . " <i class='flag-icon flag-icon-{$idioma->getIso()}'></i>",
                                'label_options' => [
                                    'disable_html_escape' => true
                                ],
                                'label_attributes' => [
                                    'class' => 'control-label col-md-3'
                                ],
                                'disable_inarray_validator' => true,
                            ],
                            'attributes' => [
                                'class' => 'form-control select2me',
                                'data-padrao' => (int)$idioma->getPadrao(),
                                'value' => isset($opcoes['options']['default']) ? $opcoes['options']['default'] : ''
                            ]
                        ];

                        if (isset($opcoes['nullable']) && !$opcoes['nullable']) {
                            $dados['attributes']['required'] = 'required';
                        }
                        if ($dados['type'] == 'text' && isset($opcoes['length']) && $opcoes['length'] > 0) {
                            $dados['attributes']['maxlength'] = $opcoes['length'];
                        }
                        if (isset($opcoes['options']['default'])) {
                            $dados['value'] = $opcoes['options']['default'];
                        }

                        $this->add($dados);
                    }
                } else {

                    $dados = [
                        'type' => $curType,
                        'name' => $coluna,
                        'options' => [
                            'label' => $this->spanAjudaPopover($opcoes['label'], $opcoes['desc']),
                            'label_options' => [
                                'disable_html_escape' => true
                            ],
                            'label_attributes' => [
                                'class' => 'control-label col-md-3'
                            ],
                            'value_options' => $this->getMultiOptions($opcoes, $coluna),
                            'disable_inarray_validator' => true,
                            'help_attrs' => $ajuda . $this->linkAjaxTarget($opcoes, $coluna),
                        ],
                        'attributes' => [
                            'class' => 'form-control select2me',
                            'value' => isset($opcoes['options']['default']) ? $opcoes['options']['default'] : ''
                        ]
                    ];

                    if (isset($opcoes['options']['default'])) {
                        $dados['value'] = $opcoes['options']['default'];
                    }

                    $this->add($dados);
                }
            } else {

                $curType = $this->getType($opcoes, $coluna);
                $dados = [
                    'type' => $curType,
                    'name' => $coluna,
                    'options' => [
                        'label' => $this->spanAjudaPopover($opcoes['label'], $opcoes['desc']),
                        'help_attrs' => $this->linkAjaxTarget($opcoes, $coluna),
                        'label_options' => ['disable_html_escape' => true],
                        'label_attributes' => ['class' => 'control-label col-md-3'],
                        'value_options' => $this->getMultiOptions($opcoes, $coluna)
                    ],
                    'attributes' => [
                        'class' => 'form-control' . ($curType == 'select' ? ' select2me' : ''),
                        'value' => isset($opcoes['options']['default']) ? $opcoes['options']['default'] : '',
                        'data-mask' => $this->getMask($opcoes['type']),
                        'data-precisao' => $opcoes['scale']
                    ]
                ];
                if (
                    !$opcoes['nullable'] &&
                    (
                        $curType != 'textarea' ||
                        (
                            isset($this->_opcoes[$coluna]['custom-attrs']['class']) &&
                            (strstr($this->_opcoes[$coluna]['custom-attrs']['class'], 'sem-tiny') ||
                                strstr($this->_opcoes[$coluna]['custom-attrs']['class'], 'tinymce-part'))
                        )
                    )
                ) {
                    $dados['attributes']['required'] = 'required';
                }
                if (($dados['type'] == 'text' || $dados['type'] == 'textarea') && isset($opcoes['length']) && $opcoes['length'] > 0) {
                    $dados['attributes']['maxlength'] = $opcoes['length'];
                }
                if (isset($opcoes['options']['default'])) {
                    $dados['value'] = $opcoes['options']['default'];
                }
                if ($dados['type'] == 'select') {
                    unset($dados['attributes']['data-mask'], $dados['attributes']['data-precisao']);
                }

                $this->add($dados);
            }

            if (isset($this->_opcoes[$coluna]['custom-attrs'])) {
                foreach ($this->_opcoes[$coluna]['custom-attrs'] as $attr => $val) {
                    $this->get($coluna)->setAttribute($attr, $val);
                }
            }

            if (isset($this->_opcoes[$coluna]['custom-field'])) {
                $this->remove($coluna);
                $this->add($this->_opcoes[$coluna]['custom-field']);
            }
        }

        if (!$this->_isAjax) {
            if ($imagens || $arquivos) {
                $this->add([
                    'type' => 'image',
                    'name' => 'nfilescripts'
                ]);
            }

            if ($imagens) {
                $this->add([
                    'type' => 'image',
                    'name' => 'nimagem',
                    'options' => [
                        'label' => 'Imagens'
                    ]
                ]);
            }

            if ($arquivos) {
                $this->add([
                    'type' => 'image',
                    'name' => 'narquivo',
                    'options' => [
                        'label' => 'Arquivos'
                    ]
                ]);
            }
        }
        /*
                // Add the CSRF field
                $this->add([
                    'type' => 'csrf',
                    'name' => 'csrf',
                    'options' => [
                        'csrf_options' => [
                            'timeout' => 600
                        ]
                    ],
                ]);
        */
        // Add the Submit button
        $this->add([
            'type' => 'button',
            'name' => 'btnSubmit',
            'options' => [
                'label' => 'Salvar <i class="fa fa-save"></i>',
                'label_options' => [
                    'disable_html_escape' => true,
                ]
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary pull-right',
                'value' => 'Salvar',
                'id' => 'btnSubmit',
            ],
        ]);

    }

    public function addCheckInput($name, $label, $is_child, $label_attr, $attr_target, $defaultCheckVal = 1, $defaultUncheckVal = 0)
    {
        return [
            'type' => 'checkbox',
            'name' => $name,
            'options' => [
                'label' => $label,
                'is_child' => $is_child,
                //'use_hidden_element' => false,
                'checked_value' => $defaultCheckVal,
                'unchecked_value' => $defaultUncheckVal,
                'label_attributes' => [
                    'class' => "control-label {$label_attr}"
                ],
            ],
            'attributes' => [
                'id' => $name,
                'class' => 'minimal',
                'data-target' => $attr_target
            ]
        ];
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter($name, $required, $filters = [], $validators = [])
    {
        $this->inputFilter->add([
            'name' => $name,
            'required' => $required,
            'allowEmpty' => true,
            'filters' => $filters,
            'validators' => $validators
        ]);
    }

    /**
     * @param array $tipo
     * @return string
     */
    private function getType(array $tipo, $coluna)
    {
        $tipos = [
            'integer' => 'text',
            'int' => 'text',
            'tinyint' => 'text',
            'decimal' => 'text',
            'float' => 'text',
            'string' => 'text',
            'varchar' => 'text',
            'char' => 'text',
            'boolean' => 'select',
            'timestamp' => 'text',
            'datetime' => 'text',
            'time' => 'text',
            'date' => 'text',
            'text' => 'textarea',
            'enum' => 'select',
            'password' => 'password',
            'file' => 'file',
            'blob' => 'file',
            'button' => 'button'
        ];

        if (isset($this->_opcoes[$coluna]) && in_array('yes-no', $this->_opcoes[$coluna], true)) {
            return 'select';
        }

        if ($tipo['fieldName'] == 'id') {
            return 'hidden';
        }

        if ($tipo['fieldName'] == 'senha') {
            return 'password';
        }

        if (isset($tipo['parent']) || $tipo['fieldName'] == 'status') {
            return 'select';
        }

        if (isset($tipo['opcoes']) && is_array($tipo['opcoes']) && isset($tipo['joinColumns']) && count($tipo['joinColumns'])) {
            return 'select';
        }

//        if (isset($tipo['parent']) || (isset($tipo['f-type']) && $tipo['f-type'] == 'normal' && $tipo['type'] != 'text')) {
//            return 'text';
//        }

        if (!isset($tipos[$tipo['type']])) {
            return 'text';
        }

        return $tipos[$tipo['type']];
    }

    /**
     * @param string $tipo
     * @return string
     */
    private function getMask(string $tipo)
    {
        $tipos = [
            'integer' => 'num-inteiro',
            'int' => 'num-inteiro',
            'tinyint' => 'num-inteiro',
            'decimal' => 'num-decimal',
            'float' => 'num-decimal',
            'string' => '',
            'varchar' => '',
            'char' => '',
            'boolean' => 'num-decimal',
            'timestamp' => 'data-datetime',
            'datetime' => 'data-datetime',
            'time' => 'data-time',
            'date' => 'data-date',
            'text' => '',
            'enum' => '',
            'password' => '',
            'file' => '',
            'blob' => '',
            'button' => ''
        ];

        return isset($tipos[$tipo]) ? $tipos[$tipo] : '';
    }

    /**
     * @param array $campo
     * @return array
     */
    private function getMultiOptions(array $campo, $coluna)
    {
        $retorno = [];

        if ($campo['f-type'] == 'foreign') {
            // Se o campo for relacionado à outra entidade
            if ($campo['nullable'] && (!$campo['parent'] || $campo['forcar'] == 'select')) {
                $retorno[''] = 'Selecione...';
            }
//            var_dump($campo);exit;

            if (is_array($campo['parent'])) {
                if ($campo['nullable'] && $campo['forcar'] !== 'select') {
                    $retorno[0] = [
                        'label' => '<small><i>(Nenhum)</i></small>',
                        'hasParent' => true,
                        'options' => []
                    ];
                }

                if ($campo['mostra_parent_vazio']) {
                    $lista = $this->_entityManager->getRepository($campo['target'])->getListaOrdenada($campo['fieldName'], 'ASC', "{$campo['parent']['fieldName']} IS NULL");
                    foreach ($lista as $item) {
                        $retorno[0]['hasParent'] = false;
                        $retorno[0]['options'][$item->getId()] = DB::get($item, $campo['fieldName']);
                    }
                }

                $hasParent = $campo['parent']['targetEntity'] == $campo['target'];
                $listaPai = $this->_entityManager->getRepository($campo['parent']['targetEntity'])->getListaOrdenada($campo['fieldName'], 'ASC', $hasParent ? "{$campo['parent']['fieldName']} IS NULL" : "");
                foreach ($listaPai as $itemPai) {
                    $retorno[$itemPai->getId()]['label'] = $itemPai->getNome();
                    $retorno[$itemPai->getId()]['hasParent'] = $hasParent;
                    $retorno[$itemPai->getId()]['options'] = [];
                    try {
                        $lista = $this->_entityManager->getRepository($campo['target'])->getListaOrdenada($campo['fieldName'], 'ASC', "{$campo['parent']['fieldName']} = {$itemPai->getId()}");
                    } catch (\Exception $e) {
                        var_dump($campo, $itemPai);
                        exit;
                    }
                    foreach ($lista as $item) {
                        $retorno[$itemPai->getId()]['options'][$item->getId()] = DB::get($item, $campo['fieldName']);
                    }
                }
            } else {
                $lista = $this->_entityManager->getRepository($campo['target'])->getListaOrdenada($campo['fieldName']);
                foreach ($lista as $item) {

                    if (isset($this->_opcoes[$coluna]['compara-coluna'])) {
                        $explode = explode('=', $this->_opcoes[$coluna]['compara-coluna']);
                        if (DB::get($item, $explode[0]) != $explode[1])
                            continue;
                    }

                    $text = '';
                    if (isset($this->_opcoes[$coluna]['lista-col'])) {
                        $cs = explode('|', $this->_opcoes[$coluna]['lista-col']);
                        foreach ($cs as $k => $c) {
                            $text .= DB::get($item, $c);
                            if (count($cs) > ($k + 1)) {
                                $text .= ' / ';
                            }
                        }
                    } elseif (isset($this->_opcoes[$coluna]['subs-form'])) {
                        $text = DB::get($item, $this->_opcoes[$coluna]['subs-form']);
                    } else {
                        $text = DB::get($item, $campo['fieldName']);
                    }
                    $retorno[$item->getId()] = $text;
                }
            }

        } elseif ($campo['f-type'] == 'idioma' && $campo['fieldName'] != 'status') {
            // Se o campo for relacionado à outra entidade
            if (isset($campo['nullable']) && $campo['nullable']) {
                $retorno[] = 'Selecione...';
            }

            if (isset($campo['opcoes']) && is_array($campo['opcoes'])) {
                if (isset($campo['nullable']) && $campo['nullable']) {
                    $retorno[] = [
                        'label' => '<small><i>(Nenhum)</i></small>',
                        'hasParent' => true,
                        'options' => []
                    ];
                }

                $cpp = isset($campo['mappedBy']) && $campo['mappedBy'] ? $campo['mappedBy'] : $campo['fieldName'];
                $listaPai = $this->_entityManager
                    ->getRepository($campo['opcoes']['sourceEntity'])
                    ->getListaOrdenadaIdioma(
                        $cpp,
                        'ASC',
                        "a1.{$cpp} = 0 OR a1.{$cpp} IS NULL"
                    );

                foreach ($listaPai as $itemPai) {

                    $idioma1 = $itemPai->getIdiomaPadrao();
                    $retorno[$itemPai->getId()]['label'] = $idioma1->getNome();
                    $retorno[$itemPai->getId()]['hasParent'] = true;
                    $retorno[$itemPai->getId()]['options'] = [];
                    foreach (\ZF3Abs\Util\DB::get($itemPai, 'children') as $children) {
                        $idioma = $children->getIdiomaPadrao();
                        $retorno[$itemPai->getId()]['options'][$idioma->getTextocategoria()->getId()] = \ZF3Abs\Util\DB::get($idioma, 'nome');
                    }
                }

            } else {
                $lista = $this->_entityManager
                    ->getRepository($campo['source'])
                    ->getListaOrdenadaIdioma($campo['columnName'], 'ASC');
                foreach ($lista as $item) {
                    $retorno[$item->getId()] = DB::get($item, $campo['fieldName']);
                }
            }

        } elseif ($campo['fieldName'] == 'status') {
            // Se for o campo de status
            $retorno = [
                '1' => 'Ativo',
                '0' => 'Inativo',
                '2' => 'Pendente',
                '3' => 'Excluído'
            ];
        } elseif ($campo['type'] == 'boolean' || (isset($this->_opcoes[$coluna]) && in_array('yes-no', $this->_opcoes[$coluna], true))) {
            // Se for um campo de sim/não
            $retorno = [
                '1' => 'Sim',
                '0' => 'Não',
            ];
        }

        return $retorno;
    }

    public function spanAjudaPopover($label = '', $desc = '')
    {
        if (!strlen($desc))
            return $label;

        $dados = [
            'data-html="true"',
            'data-toggle="popover"',
            'data-trigger="hover"',
            'data-placement="bottom"',
            'data-original-title="' . $label . '"',
            'data-content="' . $desc . '"'
        ];

        return "<span " . implode(' ', $dados) . ">{$label}</span>";
    }

    public function linkAjaxTarget(array $campo, $campoForm = false)
    {
        if (isset($this->_opcoes[$campoForm]) && is_array($this->_opcoes[$campoForm]) && in_array('sem-add', $this->_opcoes[$campoForm], true))
            return '';

        $dtNomeCampo = '';
        if ($campo['f-type'] == 'foreign') {
            $controlador = @end(explode('\\', $campo['target']));
            $dtNomeCampo = mb_strtolower("{$controlador}|{$campo['fieldName']}");
            $dtNomeCampo = "data-campo='{$dtNomeCampo}'";
        }

        $link = '';
        if (isset($campo['opcoes']) && is_array($campo['opcoes']) && isset($campo['joinColumns']) && count($campo['joinColumns']) && !$this->_isAjax) {

            $controlador = @end(explode('\\', $campo['sourceEntity']));
            $url_ajax = Utils::getFullUrl(Utils::getModuleName() . '/' . Utils::uppercaseDash($controlador) . "/adicionar");
            $link = "<a href='{$url_ajax}' class='btn bg-orange btn-flat' data-target='#ajax' {$dtNomeCampo} data-toggle='modal' role='button'><i class='fa fa-plus-circle'></i> </a>";
        } elseif (isset($campo['target']) && !$this->_isAjax) {

            if (isset($campo['f-type']) && $campo['f-type'] == 'normal') return '';

            $controlador = @end(explode('\\', $campo['target']));
            $url_ajax = Utils::getFullUrl(Utils::getModuleName() . '/' . Utils::uppercaseDash($controlador) . "/adicionar");
            $link = "<a href='{$url_ajax}' class='btn bg-orange btn-flat' data-target='#ajax' {$dtNomeCampo} data-toggle='modal' role='button'><i class='fa fa-plus-circle'></i> </a>";
        }

        return $link;
    }

}