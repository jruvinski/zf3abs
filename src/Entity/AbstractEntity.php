<?php

namespace ZF3Abs\Entity;

use Zend\Hydrator\ClassMethods;

/**
 * Class AbstractEntity
 *
 * @package Base\Entity
 */
abstract class AbstractEntity
{
    const STATUS_INATIVO = '0';
    const STATUS_ATIVO = '1';
    const STATUS_PENDENTE = '2';
    const STATUS_APAGADO = '3';

    /**
     * @var array
     */
    public $_colNome = [];

    /**
     * @var array
     */
    public $_colDesc = [];

    /**
     * @param array $options
     */
    public function __construct(Array $options = array())
    {
        $hydrator = new ClassMethods();
        $hydrator->hydrate($options, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $hydrator = new ClassMethods();
        return $hydrator->extract($this);
    }
}