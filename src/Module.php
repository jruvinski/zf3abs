<?php

namespace ZF3Abs;

use App\Service\AuthManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;
use ZF3Abs\Util\Utils;

class Module
{

    /**
     * This method returns the path to module.config.php file.
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * Event listener method for the 'Dispatch' event. We listen to the Dispatch
     * event to call the access filter. The access filter allows to determine if
     * the current visitor is allowed to see the page or not. If he/she
     * is not authorized and is not allowed to see the page, we redirect the user
     * to the login page.
     */
    public function onDispatch(MvcEvent $event)
    {
        // Get controller and action to which the HTTP request was dispatched.
        $controller = $event->getTarget();
        $moduleName = $event->getRouteMatch()->getParam('module', null);
        $controllerName = $event->getRouteMatch()->getParam('controller', null);
        $actionName = $event->getRouteMatch()->getParam('action', null);

        $services = $event->getApplication()->getServiceManager();

        $aliases = $services->get('Config')['controllers']['aliases'];

        $controllerName = $controllerName && strstr($controllerName, '\\') ? $controllerName :
            (isset($aliases[$controllerName]) ? $aliases[$controllerName] : null);

        // Convert dash-style action name to camel-case.
        $actionName = str_replace('-', '', lcfirst(ucwords($actionName, '-')));

        // Get the instance of AuthManager service.
        $authManager = $event->getApplication()->getServiceManager()->get(AuthManager::class);

        $routes = $services->get('Config')['identity_routes'];

        if (isset($routes[$moduleName])) {

            // Execute the access filter on every controller except AuthController
            // (to avoid infinite redirect).
            if ($authManager->getIdentity() && preg_match('/(AuthController)/', $controllerName) && $actionName == 'login') {

                return $controller->redirect()->toRoute($routes[$moduleName]['home']);
            } elseif (!preg_match('/(AuthController)/i', $controllerName)) {
                $result = $authManager->filterAccess($controllerName, $actionName, $moduleName);

                if ($result == AuthManager::AUTH_REQUIRED) {

                    // Remember the URL of the page the user tried to access. We will
                    // redirect the user to that URL after successful login.
                    $uri = $event->getApplication()->getRequest()->getUri();
                    // Make the URL relative (remove scheme, user info, host name and port)
                    // to avoid redirecting to other domain by a malicious user.
                    $uri
                        ->setScheme(null)
                        ->setHost(null)
                        ->setPort(null)
                        ->setUserInfo(null);
                    $redirectUrl = $uri->toString();

                    // Redirect the user to the "Login" page.
                    return $controller->redirect()->toRoute($routes[$moduleName]['login'], [], ['query' => ['redirectUrl' => $redirectUrl]]);
                } else if ($result == AuthManager::ACCESS_DENIED) {

                    // Redirect the user to the "Not Authorized" page.
                    return $controller->redirect()->toRoute($routes[$moduleName]['not-authorized']);
                }
            }
        }
    }

    public function onBootstrap(MvcEvent $e)
    {

        $app = $e->getApplication();
        $services = $app->getServiceManager();

        $eventManager = $app->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $sharedEventManager = $eventManager->getSharedManager();
        // Register the event listener method.
        $sharedEventManager->attach(AbstractActionController::class, MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], 100);

        // The following line instantiates the SessionManager and automatically
        // makes the SessionManager the 'default' one to avoid passing the
        // session manager as a dependency to other models.
        $sessionManager = $services->get(SessionManager::class);

        $eventManager->attach(MvcEvent::EVENT_ROUTE, function ($event) {
            $routeMatch = $event->getRouteMatch();
            if ($routeMatch) {
                $event->getApplication()->getMvcEvent()->getViewModel()
                    ->setVariables($routeMatch->getParams());
            }
        });

        $entityManager = $services->get('Doctrine\ORM\EntityManager');
        $entityManager->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        // Inicializa UserIdentity
        Utils::$_sv = $services;
    }

}
