<?php


namespace ZF3Abs\Util;


use PHPMailer\PHPMailer\PHPMailer;
use Wilczynski\Mailer\SendGridTransport;
use Zend\Mime\Mime;
use Zend\Serializer\Serializer;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\RendererInterface;

/**
 * Class Mail
 * @package App\Util
 */
class Mail
{
    /**
     * @var \ZF3Abs\Service\Mail
     */
    private $_mailService = null;

    /**
     * @return mixed|\ZF3Abs\Service\Mail
     */
    private function getMailService()
    {
        if (!self::$_mailService) {
            self::$_mailService = Utils::$_sv->get(\ZF3Abs\Service\Mail::class);
        }

        return self::$_mailService;
    }

    /** @var string|array */
    private $de = [];
    /** @var array */
    private $responderPara = [];
    /** @var array */
    private $para = [];
    /** @var array */
    private $copia = [];
    /** @var array */
    private $copiaOculta = [];
    /** @var string */
    private $assunto = '';
    /** @var string */
    private $mensagemHtml = '';
    /** @var string */
    private $mensagemTexto = '';
    /** @var string */
    private $template = 'zf3abs/mail/default';
    /** @var string */
    private $layout = 'zf3abs/mail/layout';
    /** @var bool */
    private $usarTemplate = true;
    /** @var array */
    private $templateVariaveis = [];
    /** @var array */
    private $anexo = [];

    /**
     * @return array|string
     */
    public function getDe()
    {
        return $this->de;
    }

    /**
     * @param $email
     * @param null $nome
     * @return Mail
     */
    public function setDe($email, $nome = null): Mail
    {
        $this->de = [];
        $this->de[$email] = $nome;
        return $this;
    }

    /**
     * @return array
     */
    public function getResponderPara(): array
    {
        if (!count($this->responderPara)) {
            $this->responderPara = $this->de;
        }
        return $this->responderPara;
    }

    /**
     * @param $email
     * @param null $nome
     * @return Mail
     */
    public function setResponderPara($email, $nome = null): Mail
    {
        $this->responderPara = [];
        $this->responderPara[$email] = $nome;
        return $this;
    }

    /**
     * @return array
     */
    public function getPara(): array
    {
        return $this->para;
    }

    /**
     * @param $email
     * @param null $nome
     * @return Mail
     */
    public function addPara($email, $nome = null): Mail
    {
        $this->para[$email] = $nome;
        return $this;
    }

    /**
     * @return array
     */
    public function getCopia(): array
    {
        return $this->copia;
    }

    /**
     * @param $email
     * @param null $nome
     * @return Mail
     */
    public function addCopia($email, $nome = null): Mail
    {
        $this->copia[$email] = $nome;
        return $this;
    }

    /**
     * @return array
     */
    public function getCopiaOculta(): array
    {
        return $this->copiaOculta;
    }

    /**
     * @param $email
     * @param null $nome
     * @return Mail
     */
    public function addCopiaOculta($email, $nome = null): Mail
    {
        $this->copiaOculta[$email] = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getAssunto(): string
    {
        return $this->assunto;
    }

    /**
     * @param string $assunto
     * @return Mail
     */
    public function setAssunto(string $assunto): Mail
    {
        $this->assunto = $assunto;
        return $this;
    }

    /**
     * @return string
     */
    public function getMensagemHtml(): string
    {
        return $this->mensagemHtml;
    }

    /**
     * @param string $mensagemHtml
     * @return Mail
     */
    public function setMensagemHtml(string $mensagemHtml): Mail
    {
        $this->mensagemHtml = $mensagemHtml;
        return $this;
    }

    /**
     * @return string
     */
    public function getMensagemTexto(): string
    {
        return $this->mensagemTexto;
    }

    /**
     * @param string $mensagemTexto
     * @return Mail
     */
    public function setMensagemTexto(string $mensagemTexto): Mail
    {
        $this->mensagemTexto = strip_tags($mensagemTexto);
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param string $template
     * @return Mail
     */
    public function setTemplate(string $template): Mail
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return string
     */
    public function getLayout(): string
    {
        return $this->layout;
    }

    /**
     * @param string $layout
     * @return Mail
     */
    public function setLayout(string $layout): Mail
    {
        $this->layout = $layout;
        return $this;
    }

    /**
     * @return bool
     */
    public function getUsarTemplate(): bool
    {
        return $this->usarTemplate;
    }

    /**
     * @param bool $usarTemplate
     * @return Mail
     */
    public function setUsarTemplate(bool $usarTemplate): Mail
    {
        $this->usarTemplate = $usarTemplate;
        return $this;
    }

    /**
     * @return array
     */
    public function getTemplateVariaveis(): array
    {
        return $this->templateVariaveis;
    }

    /**
     * @param array $templateVariaveis
     * @return Mail
     */
    public function setTemplateVariaveis(array $templateVariaveis): Mail
    {
        $this->templateVariaveis = $templateVariaveis;
        return $this;
    }

    /**
     * @return array
     */
    public function getAnexo(): array
    {
        return $this->anexo;
    }

    /**
     * @param string $anexo
     * @return Mail
     */
    public function addAnexo(string $anexo): Mail
    {
        $this->anexo[] = $anexo;
        return $this;
    }

    public function enviar()
    {
        $transporte = $this->validaEnvio();
        if (is_array($transporte)) {
            throw new \Exception("Alguns erros foram encontrados:" . implode(PHP_EOL, $transporte));
        }

        $mail = $this->validaEnvio();

        if (!count($this->getDe())) {
            $this->setDe(DB::Config('CONTATO_EMAIL_USAR'), DB::Config('SEO_TITLE'));
        }

        if (!count($this->getResponderPara())) {
            $this->setResponderPara(DB::Config('CONTATO_EMAIL_MOSTRAR'), DB::Config('SEO_TITLE'));
        }

        $deNome = array_keys($this->getDe());
        $mail->setFrom($deNome[0], $this->getDe()[$deNome[0]]);

        $replyNome = array_keys($this->getResponderPara());
        $mail->addReplyTo($replyNome[0], $this->getResponderPara()[$replyNome[0]]);

        foreach ($this->getPara() as $email => $nome) {
            $mail->addAddress($email, $nome);
        }

        foreach ($this->getCopia() as $email => $nome) {
            $mail->addCC($email, $nome);
        }

        foreach ($this->getCopiaOculta() as $email => $nome) {
            $mail->addBCC($email, $nome);
        }

        $mail->Subject = $this->getAssunto();

        if (count($this->getTemplateVariaveis())) {

            $renderer = Utils::$_sv->get(RendererInterface::class);

            if (!isset($this->templateVariaveis['assunto'])) {
                $this->templateVariaveis['assunto'] = $this->getAssunto();
            }

            $content = new ViewModel($this->getTemplateVariaveis());
            $content->setTemplate($this->getTemplate());

            $layout = new ViewModel(['conteudo' => $renderer->render($content)]);
            $layout->setTemplate($this->getLayout());

            $this->setMensagemHtml($renderer->render($layout));
        }

        if (strlen($this->getMensagemHtml())) {
            $mail->isHTML(true);
            $mail->Body = $this->getMensagemHtml();

            if (strlen($this->getMensagemTexto())) {
                $mail->AltBody = $this->getMensagemTexto();
            }
        } elseif (strlen($this->getMensagemTexto())) {
            $mail->isHTML(false);
            $mail->Body = $this->getMensagemTexto();
        }

        foreach ($this->getAnexo() as $anexo) {
            $mail->addAttachment($anexo);
        }

        try {

            $log = Serializer::serialize([
                'to' => implode(',', array_keys($this->getPara())),
                'subject' => $this->getAssunto(),
                'content_html' => $this->getMensagemHtml(),
                'content_text' => $this->getMensagemTexto(),
                'attachment' => $this->getAnexo()
            ]);
            $pastaLog = ROOT_PATH . '/data/log_mails/' . Utils::DateTime()->format('Y') . '/' . Utils::DateTime()->format('m') . '/' . Utils::DateTime()->format('d') . '/';
            if (!is_dir($pastaLog)) {
                mkdir($pastaLog, 0777, true);
            }
            file_put_contents($pastaLog . Utils::urlAmigavel(Utils::DateTime()->format('His\.u') . '_' . $this->getAssunto()), $log);

            $resultado = $mail->send();
            return [
                'status' => (bool)$resultado,
                'msg' => ((bool)$resultado) ? 'Mensagem enviada com sucesso!' : $mail->ErrorInfo
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'msg' => $e->getMessage()
            ];
        }
    }

    /**
     * @return array|PHPMailer
     */
    private function validaEnvio()
    {
        $erros = [];

        $mail = new PHPMailer();
        $mail->CharSet = PHPMailer::CHARSET_UTF8;
        $mail->XMailer = DB::Config('SEO_TITLE') . " (" . Utils::getFullUrl('') . ")";

        switch (DB::ConfigD('MAIL_TRANSPORT', 'sendmail')) {
            case 'smtp':

                $host = DB::Config('MAIL_HOST');
                $user = DB::Config('MAIL_USUARIO');
                $pass = DB::Config('MAIL_SENHA');
                $port = DB::Config('MAIL_PORTA');
                $ssl = DB::ConfigD('MAIL_SSL', '');

                if (!$host || !$user || !$pass || !$port) {
                    $erros[] = 'Verifique se a configuração SMTP está completa.';
                }

                $mail->isSMTP();
                $mail->SMTPAuth = true;
                $mail->SMTPDebug = DB::Config('MAIL_DEBUG');
                $mail->Host = $host;
                $mail->Username = $user;
                $mail->Password = $pass;
                $mail->Port = $port;
                $mail->SMTPSecure = $ssl;

                break;
            case 'sendmail':
                $mail->isSendmail();
                break;
            default:
                $mail->isMail();
                break;
        }

        if (!strlen($this->getAssunto())) {
            $erros[] = 'A mensagem precisa ter um assunto.';
        }

        if (!strlen($this->getMensagemHtml()) && !strlen($this->getMensagemTexto()) && !count($this->getTemplateVariaveis())) {
            $erros[] = 'É preciso ter uma mensagem, html, texto ou adicionar as variáveis do template.';
        }

        if (!count($this->getPara())) {
            $erros[] = 'É preciso pelo menos um destinatário.';
        }

        return count($erros) ? $erros : $mail;
    }
}