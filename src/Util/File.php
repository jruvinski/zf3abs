<?php

namespace ZF3Abs\Util;

use App\Entity\Tamanhos;
use App\Repository\ControladorArquivoRepository;
use App\Repository\TamanhosRepository;
use Cocur\Slugify\Slugify;
use Zend\View\Renderer\PhpRenderer;

class File
{

    public static $IMG_PATH = ROOT_PATH . '/public/assets/application/img/upload/';

    /**
     * Retorna url completa
     *
     * @param string $url
     * @return string
     */
    public static function getFullUrl($url = '')
    {
        $http = substr($url, 0, 1) !== "/" ? "/{$url}" : $url;

        $vw = new PhpRenderer();
        return $vw->serverUrl($http);
    }

    /**
     * Recursive glob function
     * @param $pattern
     * @param int $flags
     * @return array|false
     */
    public static function rglob($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
        foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
            $files = array_merge($files, self::rglob($dir . '/' . basename($pattern), $flags));
        }
        return $files;
    }

    /**
     * Remove as imagens geradas a partir da imagem original
     * @param string $tamanho
     */
    public static function removerImagensGeradas($tamanho = '')
    {
        foreach (self::rglob(ROOT_PATH . "/public/assets/application/img/upload/*-{$tamanho}*") as $arq) {
            @unlink($arq);
        }
    }

    /**
     * Retorna o nome do arquivo
     * @param string $path
     * @return string
     */
    public static function getFileName($path, $type = 'I')
    {
        $pathExploded = explode($type == 'I' ? 'upload' : 'files', $path);
        $idArquivo = str_replace('/', '', $pathExploded[count($pathExploded) - 1]);

        $arquivos = glob("{$path}{$idArquivo}.*");
        if (!count($arquivos)) return false;

        $pathExploded = explode('/', $arquivos[0]);
        $nomeArquivo = end($pathExploded);

        return $nomeArquivo;
    }

    /**
     * Retorna o tipo de arquivo
     * @param string $path
     * @return string
     */
    public static function getFileExt($path, $tamanho = false)
    {
        $pathExploded = explode('.', $path);
        $ext = end($pathExploded);

        if ($tamanho && $tamanho != 'file') {
            $pathSemExt = str_replace(".{$ext}", '', $path);

            $combinacao = glob("{$pathSemExt}-{$tamanho}*");
            if (count($combinacao)) {
                $pathExploded = explode('.', $combinacao[0]);
                $ext = end($pathExploded);
            }
        }

        return $ext;
    }

    /**
     * Retorna o caminho até o arquivo salvo, ou onde será salvo o arquivo
     * @param string|int $id
     * @param string $type
     * @return string
     */
    public static function getFileSavePath($id, $type = 'I', $nomeArquivo = false, $upload = false)
    {
        if ($upload) {

            $nome = $id;
            $id = @reset(explode('.', $id));

            $idExploded = implode('/', str_split($id));
            $path = self::$IMG_PATH . ($type == 'A' ? 'files/' : '') . "{$idExploded}/";

            if (!is_dir($path)) {
                @mkdir($path, 0777, true);
            }

            if ($nomeArquivo) {
                $path .= $nome;
            }

            return $path;
        }

        if (!is_numeric($id)) return false;

        $idExploded = implode('/', str_split($id));
        $path = self::$IMG_PATH . ($type == 'A' ? 'files/' : '') . "{$idExploded}/";

        if (!is_dir($path)) {
            @mkdir($path, 0777, true);
        }

        if ($nomeArquivo) {
            $path .= self::getFileName($path, $type);
        }

        return $path;
    }

    /**
     * Retorna o caminho correto até a imagem
     * @param int|string $id
     * @param string|null $tamanho
     * @param string|false $label
     * @return string URL gerado para a imagem
     * @throws \Exception
     */
    public static function getImageUrl($id, $tamanho = null, $label = false)
    {
        if (!$label) {
            $label = Utils::Setup('SEO_TITLE');
        }

        if ($id === null) {
            $em = Utils::getEntityManager();
            /** @var $mArquivo TamanhosRepository */
            $rTamanhos = $em->getRepository(\App\Entity\Tamanhos::class);

            $w = $h = 180;

            /** @var $tamanho Tamanhos */
            foreach ($rTamanhos->findAll() as $tamanhoDB) {
                if ((new Slugify())->slugify($tamanhoDB->getNome()) == $tamanho) {
                    $w = $tamanhoDB->getLargura();
                    $h = $tamanhoDB->getAltura();
                }
            }

            if(!is_file(ROOT_PATH."/assets/site/img/tmp/{$w}-{$h}.png")) {
                $verot = new Verot(ROOT_PATH . '/public/assets/site/img/logo.png', 'pt_BR');
                $verot->file_overwrite = true;
                $verot->file_new_name_body = "{$w}-{$h}";
                $verot->image_resize = true;
                $verot->image_interlace = true;
                $verot->image_y = $h;
                $verot->image_x = $w;
                $verot->image_ratio_fill = true;
                $verot->image_ratio_crop = !$verot->image_ratio_fill;
                $verot->png_compression = 9;

                $verot->Process(ROOT_PATH . '/public/assets/site/img/tmp/');
            }

            return self::getFullUrl("assets/site/img/tmp/{$w}-{$h}.png");
        }

        if (!is_numeric($id)) {
            $id = str_replace("." . self::getFileExt($id), '', $id);
        }

        $filePath = self::getFileSavePath($id, $tamanho == 'file' ? 'A' : 'I', true);
        $ext = self::getFileExt($filePath, $tamanho);
        $label = Utils::urlAmigavel($label, '');
        $tamanho2 = $tamanho && $tamanho != 'file' ? "-{$tamanho}" : "";
        $label = str_replace($ext, '', $label);

        // Se o tamanho da imagem não existe ela é gerada
        $tamanhoAlternativo = str_replace(".{$ext}", '', $filePath) . "-" . $tamanho . ".{$ext}";
        if ($tamanho != 'file' && strlen($tamanho) && !is_file($tamanhoAlternativo)) {

            self::remakeImage([$id], $tamanho);
            $ext = self::getFileExt($filePath, $tamanho);
        }

        $url = self::getFullUrl(($tamanho == 'file' ? 'arq/' : 'img/') . "{$id}{$tamanho2}/{$label}.{$ext}");

        return $url;
    }

    public static function getCapa($controller, $registro, $tamanho, $label)
    {
        $em = Utils::getEntityManager();

        /** @var $mArquivo ControladorArquivoRepository */
        $mArquivo = $em->getRepository(\App\Entity\ControladorArquivo::class);

        if ($imagem = $mArquivo->getCapa($controller, $registro)) {

            return self::getImageUrl($imagem->getArquivo()->getId(), $tamanho, $label);
        }

        return self::getImageUrl(null, $tamanho, $label);
    }

    public static function getGaleria($controller, $registro, $capa = false)
    {
        $em = Utils::getEntityManager();

        /** @var $mArquivo ControladorArquivoRepository */
        $mArquivo = $em->getRepository(\App\Entity\ControladorArquivo::class);

        return $mArquivo->getGaleria($controller, $registro, $capa);
    }

    public static function getArquivos($controller, $registro)
    {
        $em = Utils::getEntityManager();

        /** @var $mArquivo ControladorArquivoRepository */
        $mArquivo = $em->getRepository(\App\Entity\ControladorArquivo::class);

        return $mArquivo->getArquivos($controller, $registro);
    }

    public static function getImageLink($controller, $registro, $tamanho, $label)
    {
        $em = Utils::getEntityManager();

        /** @var $mArquivo ControladorArquivoRepository */
        $mArquivo = $em->getRepository(\App\Entity\ControladorArquivo::class);

        if ($imagem = $mArquivo->getCapa($controller, $registro)) {

            return self::getImageUrl($imagem->getArquivo()->getId(), $tamanho, $label);
        }

        return self::getImageUrl(null, $tamanho, $label);
    }

    private static function hasAlpha($img)
    {
        return (ord(@file_get_contents($img, NULL, NULL, 25, 1)) == 6);
    }

    /**
     * @param $ids
     * @param string $tm
     * @return string
     */
    public static function remakeImage($ids, $tm = 'thumbs')
    {
        $em = Utils::getEntityManager();

        $mTamanho = $em->getRepository(\App\Entity\Tamanhos::class);
        $mArquivo = $em->getRepository(\App\Entity\Arquivo::class);

        foreach ($ids as $id_image) {

            //Carrega imagem atual
            $image = $mArquivo->findOneBy(['id' => $id_image]);

            // Não tem como redimensionar arquivo, então pulamos caso o tipo seja A
            if ($image->getTipo() == 'A')
                continue;

            foreach ($mTamanho->findBy(['status' => '1'], ['nome' => 'ASC']) as $tamanho) {
                /*if (\App\Util\Utils::urlAmigavel($tamanho->getNome()) == $tm)
                    break;*/
                if (Utils::urlAmigavel($tamanho->getNome()) != $tm)
                    continue;

                $pathComNome = self::getFileSavePath($image->getId(), 'I', true);
                $pathSemNome = self::getFileSavePath($image->getId(), 'I');

                $verot = new Verot($pathComNome, 'pt_BR');
                $verot->file_new_name_body = $image->getId() . "-" . Utils::urlAmigavel($tamanho->getNome());
                $verot->file_overwrite = true;
                $verot->image_resize = true;
                $verot->image_interlace = true;

                if (DB::Config('CONVERTER_ALPHA_PARA_GIF') && self::hasAlpha($pathComNome)) {
                    $verot->image_convert = 'gif';
                } elseif (DB::Config('CONVERTER_NAO_ALPHA_PARA_JPG') && !self::hasAlpha($pathComNome)) {
                    $verot->image_convert = 'jpg';
                }

                if (DB::Config('CONVERTER_TUDO_JPG')) {
                    $verot->image_convert = 'jpg';
                    $verot->image_background_color = DB::Config('CONVERTER_TUDO_JPG_FUNDO');
                }

                $verot->image_y = $tamanho->getAltura();
                $verot->image_x = $tamanho->getLargura();
                $verot->image_ratio_fill = (boolean)$tamanho->getBorda();
                $verot->image_ratio_crop = !$verot->image_ratio_fill;
                $verot->image_default_color = '#FFF';
                $verot->png_compression = 9;
                $verot->jpeg_quality = 70;
                $verot->webp_quality = 75;

                // Se a imagem enviada for menor que o tamanho de destino não é redimensionada nem cortada
                if ($verot->image_x > $verot->image_src_x && $verot->image_y > $verot->image_src_y && !((boolean)$tamanho->getRedimensionarPequena())) {
                    $borda_x = round(($verot->image_x - $verot->image_src_x) / 2);
                    $borda_y = round(($verot->image_y - $verot->image_src_y) / 2);

                    $verot->image_resize = false;
                    $verot->image_border = "{$borda_y} {$borda_x}";
                    $verot->image_border_opacity = 0;
                }

                // Colocar marca d'água no tamanho proporcional à imagem redimensionada
                if (DB::Config('IMG_ADD_MARCA_DAGUA')) {

                    $imagemPadraoL = 559;
                    $imagemPadraoA = 461;

                    $marcaDaguaSobreImagemPadrao = 98;

                    $distanciaMarcaDir = 45;
                    $distanciaMarcaBai = 50;

                    ### Não mexer daqui pra baixo

                    $novaImagemL = $tamanho->getLargura();
                    $novaImagemA = $tamanho->getAltura();

                    $razaoMedidaPadraoImagem = $imagemPadraoL * $imagemPadraoA;
                    $razaoMedidaPadraoMarcaD = $marcaDaguaSobreImagemPadrao * $marcaDaguaSobreImagemPadrao;
                    $razaoMedidaNovaImagem = $novaImagemL * $novaImagemA;

                    $razaoDistanciaPadraoX = $distanciaMarcaDir * $distanciaMarcaDir;
                    $razaoDistanciaPadraoY = $distanciaMarcaBai * $distanciaMarcaBai;

                    $proporcao = $razaoMedidaPadraoMarcaD / $razaoMedidaPadraoImagem;
                    $proporcaoX = $razaoDistanciaPadraoX / $razaoMedidaPadraoImagem;
                    $proporcaoY = $razaoDistanciaPadraoY / $razaoMedidaPadraoImagem;

                    $novaMedidaMarcaDagua = round(sqrt($razaoMedidaNovaImagem * $proporcao));

                    $imgMarcaPath = ROOT_PATH . '/public/assets/application/img/';
                    $watermark = new Verot();
                    $watermark->upload("{$imgMarcaPath}marcadagua.png", 'pt_BR');
                    $watermark->file_new_name_body = 'marcadagua_tmp';
                    $watermark->image_resize = true;
                    $watermark->file_overwrite = true;
                	$watermark->image_interlace = true;
                    $watermark->image_y = $novaMedidaMarcaDagua;
                    $watermark->image_x = $novaMedidaMarcaDagua;
                    $watermark->png_compression = 9;
                    $watermark->Process($imgMarcaPath);

                    $verot->image_watermark = "{$imgMarcaPath}marcadagua_tmp.png";
                    $verot->image_watermark_x = round(sqrt($razaoMedidaNovaImagem * $proporcaoX)) * -1;
                    $verot->image_watermark_y = round(sqrt($razaoMedidaNovaImagem * $proporcaoY)) * -1;
                }

                $verot->Process($pathSemNome);
            }
        }

        // Se estiver marca d'agua temporária ela será apagada
        @unlink("{$imgMarcaPath}marcadagua_tmp.png");

        return $verot->file_dst_path;
    }

    public static function humanFilesize($id, $tipo, $decimals = 2)
    {

        $bytes = filesize(self::getFileSavePath($id, $tipo, true));
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor] . 'B';
    }

    public static function return_bytes($val)
    {
        $val = trim($val);

        if (is_numeric($val))
            return $val;

        $last = strtolower($val[strlen($val) - 1]);
        $val = substr($val, 0, -1); // necessary since PHP 7.1; otherwise optional

        switch ($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }

        return $val;
    }
}