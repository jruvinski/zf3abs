<?php

namespace ZF3Abs\Util;


class SEO
{

    /**
     * Configura as tags de compartilhamento
     * $dados = array('title' => '', 'description' => '', 'image' => '');
     *
     * @param $view
     * @param bool $dados
     * @throws \Exception
     */
    public static function setHeadMeta($view, $dados = false)
    {
        $title = ($dados['title'] ? "{$dados['title']} | " : '') . DB::Config('SEO_TITLE');
        $description = ($dados['description'] ? self::substrws(str_replace(array('\n', PHP_EOL, '  ', '\t'), ' ', strip_tags(html_entity_decode($dados['description'])))) . ' | ' : '') . DB::Config('SEO_TITLE');
        $url = self::getFullUrl($view->serverUrl(true));
        $metaImagem = isset($dados['image']) && $dados['image'] ?
            $dados['image'] : SEO::getFacebookLogo();

        $view->headMeta()->setName('site_name', $setup->seo_title);
        $view->headMeta()->setName('title', $title);
        $view->headMeta()->setName('description', $description);
        $view->headMeta()->setName('url', $url);
        $view->headMeta()->setName('image', $metaImagem);
        $view->headMeta()->setName('locale', 'pt_BR');

        $view->headMeta()->setName('og:site_name', $setup->seo_title);
        $view->headMeta()->setName('og:title', $title);
        $view->headMeta()->setName('og:description', $description);
        $view->headMeta()->setName('og:url', $url);
        $view->headMeta()->setName('og:image', $metaImagem);
        $view->headMeta()->setName('og:locale', 'pt_BR');

        $view->doctype('XHTML1_RDFA');
        $view->headMeta()->setProperty('og:site_name', $setup->seo_title);
        $view->headMeta()->setProperty('og:title', $title);
        $view->headMeta()->setProperty('og:description', $description);
        $view->headMeta()->setProperty('og:url', $url);
        $view->headMeta()->setProperty('og:image', $metaImagem);

        $view->headTitle($dados['title']);
    }

    public static function getLogo($tipo = 'LOGO_SITE')
    {
        $pathLogo = ROOT_PATH . '/public/assets/site/img/logo/' . DB::Config($tipo);
        if (!is_file($pathLogo)) {
            return Utils::getFullUrl('imagem/logo/' . ($tipo == 'LOGO_SITE' ? 'default-normal.png' : 'default-mini.png'));
        }

        return Utils::getFullUrl('imagem/logo/' . DB::Config($tipo));
    }

    public static function getFacebookLogo()
    {
        $pathLogo = ROOT_PATH . '/public/assets/site/img/facebook/' . DB::Config('LOGO_FACEBOOK');
        if (!is_file($pathLogo)) {
            return Utils::getFullUrl('imagem/facebook/default-facebook.jpg');
        }

        return Utils::getFullUrl('imagem/facebook/' . DB::Config('LOGO_FACEBOOK'));
    }

    public static function getFavicon()
    {
        $pathLogo = ROOT_PATH . '/public/assets/site/img/favicon/' . DB::Config('LOGO_FAVICON');
        if (!is_file($pathLogo)) {
            return Utils::getFullUrl('imagem/favicon/default-favicon.ico');
        }

        return Utils::getFullUrl('imagem/favicon/' . DB::Config('LOGO_FAVICON'));
    }

}