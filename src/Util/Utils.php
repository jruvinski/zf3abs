<?php

namespace ZF3Abs\Util;

use App\Entity\Configs;
use App\Entity\Usuario;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Zend\View\Helper\Placeholder\Container;
use Zend\View\Renderer\PhpRenderer;
use App\Service\AuthManager;

class Utils
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private static $_em;

    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    public static $_sv;

    /**
     * @var array
     */
    private static $_setup;

    /**
     * @var bool
     */
    private static $_headMetaConfigurado = false;

    /**
     * @var null|string
     */
    public static $_identity = null;

    /**
     * @var \App\Entity\Usuario|null
     */
    public static $_usuario = null;

    /**
     * @return EntityManager
     */
    public static function getEntityManager(): EntityManager
    {
        if (!self::$_em) {

            self::$_em = self::$_sv->get('Doctrine\ORM\EntityManager');
        }

        return self::$_em;
    }

    /**
     * @return string
     */
    public static function getModuleName()
    {
        return self::$_sv->get('router')->match(self::$_sv->get('request'))->getParam('module');
    }

    /**
     * @return Usuario|null|object
     */
    public static function getIdentity()
    {

        $repositorios = [
            'painel' => Usuario::class,
        ];
        $module = self::getModuleName();

        if (!self::$_identity) {
            self::$_identity = self::$_sv->get(AuthManager::class)->getIdentity();
        }

        if (self::$_usuario == null && self::$_identity) {
            $em = self::getEntityManager();
            $mUsuario = $em->getRepository($repositorios[$module]);

            self::$_usuario = $mUsuario->findOneBy(['email' => self::$_identity]);
        }

        return self::$_usuario;
    }

    /**
     * @param $setup
     * @return Usuario|null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public static function salvaSetup($setup)
    {

        if (self::$_usuario) {
            $em = self::getEntityManager();
            $mUsuario = $em->getRepository(Usuario::class);

            $user = $mUsuario->find(self::$_usuario->getId());
            $user->setSetup($setup);
            $em->persist($user);
            $em->flush();

            self::$_usuario = $user;
        }
        return self::$_usuario;
    }

    /**
     * Transforma uma string comum em uma url convencional
     *
     * @param String $str
     * @param String $ext
     * @return String
     */
    public static function urlAmigavel($str, $ext = ''): String
    {
        $nstr = (new Slugify())->slugify(trim($str));

        if (strlen($ext)) {
            $nstr .= str_replace('.', '', $ext);
        }

        return $nstr;
    }

    public static function uppercaseDash(string $str, $replacement = '-')
    {

        return strtolower(preg_replace('/([A-Z]+)/', "{$replacement}$1", lcfirst($str)));
    }

    public static function iniciaisLetras(string $str, $replacement = '_')
    {
        $ini = '';
        foreach (explode($replacement, $str) as $palavra) {
            $ini .= str_split($palavra)[0];
        }

        return $ini;
    }

    /**
     * Retorna a url completa com schema e host
     *
     * @param String $url
     * @return String
     */
    public static function getFullUrl($url = ''): String
    {

        $http = substr($url, 0, 1) !== "/" ? "/{$url}" : $url;

        $view = new PhpRenderer();

        return $view->serverUrl(ROOT_URL . $http);
    }

    /**
     * Remove os acentos da string enviada
     *
     * @param String $str
     * @return String
     */
    public static function removeAcentos(String $str): String
    {

        return (new Slugify())->slugify(trim($str), ' ');
    }

    /**
     * Retorna parte da string, sem cortar palavras nem tags html...
     *
     * @param string $text
     * @param int $len
     * @param string $suffix
     * @return string
     */
    public static function substrws($text, $len = 140, $suffix = '...')
    {

        if ((mb_strlen($text) > $len)) {
            $whitespaceposition = mb_strpos($text, " ", $len) - 1;
            if ($whitespaceposition > 0) {
                $chars = count_chars(mb_substr($text, 0, ($whitespaceposition + 1)), 1);
                if (array_key_exists(ord('<'), $chars) && array_key_exists(ord('>'), $chars)) {
                    if ($chars[ord('<')] > $chars[ord('>')]) {
                        $whitespaceposition = mb_strpos($text, ">", $whitespaceposition) - 1;
                    }
                }
                $text = mb_substr($text, 0, ($whitespaceposition + 1));
            }
            // fecha as tags abertas
            if (preg_match_all("|<([a-zA-Z]+)|", $text, $aBuffer)) {
                if (!empty($aBuffer[1])) {
                    preg_match_all("|</([a-zA-Z]+)>|", $text, $aBuffer2);
                    if (count($aBuffer[1]) != count($aBuffer2[1])) {
                        foreach ($aBuffer[1] as $index => $tag) {
                            if (empty($aBuffer2[1][$index]) || $aBuffer2[1][$index] != $tag)
                                $text .= '</' . $tag . '>';
                        }
                    }
                }
            }
            $text .= $suffix;
        }
        return $text;
    }

    /**
     * Gera uma string aleatória que pode ser usada como senha
     *
     * @param Int $tamanho
     * @param bool $maiusculas
     * @param bool $numeros
     * @param bool $simbolos
     * @return string
     */
    public static function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
    {

        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num = '1234567890';
        $simb = '!@#$%*-';
        $retorno = '';
        $caracteres = '';

        $caracteres .= $lmin;
        if ($maiusculas)
            $caracteres .= $lmai;
        if ($numeros)
            $caracteres .= $num;
        if ($simbolos)
            $caracteres .= $simb;

        $len = strlen($caracteres);
        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand - 1];
        }

        return $retorno;
    }

    /**
     * Retorna o conteúdo que estiver entre determinadas strings
     *
     * @param String $string
     * @param String $start
     * @param String $end
     * @return bool|string
     */
    public static function get_string_between($string, $start, $end)
    {

        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;

        return substr($string, $ini, $len);
    }

    /**
     * Mascara uma string de acordo o o padrão que for passado
     *
     * @param String $val
     * @param String $mask
     * @return String
     */
    public static function maskString($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    /**
     * Tranforma todos os itens do array em objetos
     *
     * @param array $array
     * @return \stdClass
     */
    public static function array_to_object(Array $array)
    {

        $obj = new \stdClass();
        foreach ($array as $k => $v) {
            if (strlen($k)) {
                if (is_array($v)) {
                    $obj->{$k} = self::array_to_object($v);
                } else {
                    $obj->{$k} = $v;
                }
            }
        }

        return $obj;
    }

    /**
     * Configura as tags de compartilhamento
     * $dados = array('title' => '', 'description' => '', 'image' => '');
     *
     * @param $view
     * @param array $dados
     * @throws \Exception
     */
    public static function setHeadMeta($view, $dados = [])
    {

        if (self::$_headMetaConfigurado)
            return;

        self::$_headMetaConfigurado = true;

        $title = (APPLICATION_ENV == 'development' ? "(" . APPLICATION_ENV . ") " : '') . (isset($dados['title']) && $dados['title'] ? "{$dados['title']} | " : '') . DB::Config('SEO_TITLE');
        $description = (isset($dados['description']) && $dados['description'] ? self::substrws(str_replace(array("\n", PHP_EOL, '  ', '\t'), ' ', strip_tags(html_entity_decode($dados['description'])))) : DB::Config('SEO_DESCRIPTION')) . ' | ' . DB::Config('SEO_TITLE');
        $url = $view->serverUrl(true);

        if (isset($dados['image']) && $dados['image'] && !strstr($dados['image'], 'picsum')) {
            if (is_numeric($dados['image'])) {
                $metaImagem = File::getImageUrl($dados['image'], 'facebook', $title);
            } else {
                $metaImagem = $dados['image'];
            }
        } else {
            $metaImagem = SEO::getFacebookLogo();
        }

        $view->headMeta()
            ->setName('site_name', DB::Config('SEO_TITLE'))
            ->setName('title', $title)
            ->setName('description', $description)
            ->setName('url', $url)
            ->setName('image', $metaImagem)
            ->setName('locale', 'pt_BR')
            ->setName('mobile-web-app-capable', 'yes')
            ->setName('apple-mobile-web-app-capable', 'yes')
            ->setName('apple-mobile-web-app-title', $title);

        $view->headMeta()
            ->setName('og:site_name', DB::Config('SEO_TITLE'))
            ->setName('og:title', $title)
            ->setName('og:description', $description)
            ->setName('og:url', $url)
            ->setName('og:image', $metaImagem)
            ->setName('og:image:secure_url', $metaImagem)
            ->setName('og:image:secure_url', $metaImagem)
            ->setName('og:image:width', 720)
            ->setName('og:image:height', 408)
            ->setName('og:locale', 'pt_BR');

        $view->doctype('XHTML1_RDFA');
        $view->headMeta()
            ->setProperty('og:site_name', DB::Config('SEO_TITLE'))
            ->setProperty('og:title', $title)
            ->setProperty('og:description', $description)
            ->setProperty('og:url', $url)
            ->setProperty('og:image', $metaImagem)
            ->setProperty('og:image:secure_url', $metaImagem)
            ->setProperty('og:image:width', 720)
            ->setProperty('og:image:height', 408);

        $view->headTitle($title, Container::SET)->setSeparator(' | ')->setAutoEscape(false);
    }

    /**
     * Retorna a string minificada
     *
     * @param string $str
     * @return string
     */
    public static function compressHtml($str)
    {

        $search = array(
            "/ +/" => " ",
            "/<!--\[if(.*)|(.*)if\]-->|<!--(.*?)-->|\/\/<!--|\/\/-->|[\t\r\n]|<!--|-->|\/\/ <!--|\/\/ -->|<!--\[CDATA\[|\/\/ \]\]-->|\]\]&gt;|\/\/<!\[CDATA\[|\/\/\]\]>|\]\]&gt;|\/\/\]\]&gt;|\/\/<!--\[CDATA\[/" => ""
        );
        if (APPLICATION_ENV == 'production') {

            $str = preg_replace(array_keys($search), array_values($search), trim($str));
            $subs = array(
                '> <' => '><'
            );
            $str = strtr($str, $subs);
        }

        return $str;
    }

    public static function loadJsLang($scr)
    {
        switch (self::$_translator->getLocale()) {
            case 'en' :
                $jsLang = [
                    'tinyMCE' => 'en',
                    'validation' => 'en',
                ];
                break;
            default:
                $jsLang = [
                    'tinyMCE' => 'pt_BR',
                    'validation' => 'pt_BR',
                ];
                break;
        }

        return $jsLang[$scr];
    }

    /**
     * Recupera item de configuração
     *
     * @param string $conf
     * @return string
     * @throws \Exception
     */
    public static function Setup($conf)
    {

        if (isset(self::$_setup[$conf])) {

            return self::$_setup[$conf];
        }

        $em = self::getEntityManager();

        $row = $em->getRepository(Configs::class)->findOneBy(['nome' => $conf]);
        if ($row) {

            self::$_setup[$conf] = $row->getValor();
            return self::$_setup[$conf];
        }

        throw new \Exception("A configuração '$conf' não existe");
    }

    /**
     * Recupera o ID do vídeo do youtube
     *
     * @param string $link
     * @return string
     */
    public static function getCodigoVideoYoutube($link)
    {
        preg_match('/(http(s|):|)\/\/(www\.|)yout(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $link, $results);

        return $results[6];
    }

    public static function timeAgo($datetime, $full = false)
    {
        $now = Utils::DateTime();
        $ago = $datetime instanceof \DateTime ? $datetime : Utils::DateTime($datetime);
        $diff = $now->diff($ago);

        $string = array(
            'y' => 'ano',
            'm' => 'mês',
            'd' => 'dia',
            'h' => 'hora',
            'i' => 'minuto',
            's' => 'segundo',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
        if (count($string) > 3 && isset($string['d'])) {
            $dias = @reset(explode(' ', $string['d']));
            if ($dias > 5) {
                return $datetime->format('d/m/Y \à\s H:i');
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' atrás' : 'agora';
    }

    public static function DateTime($time = 'now')
    {
        return new \DateTime($time, new \DateTimeZone('America/Sao_Paulo'));
    }
}
