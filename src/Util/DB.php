<?php

namespace ZF3Abs\Util;

use App\Entity\Configs;

class DB
{

    /**
     * @var \App\Repository\IdiomaRepository|null
     */
    private static $_idiomas = null;

    /**
     * @var \App\Repository\ConfigsRepository|null
     */
    private static $_config = null;

    /**
     * @param \Doctrine\ORM\PersistentCollection|\ZF3Abs\Repository\AbstractRepository $repositorio
     * @param string $get
     * @return \ZF3Abs\Repository\AbstractRepository|string
     */
    public static function get($repositorio, $get, $idiomaPadrao = true)
    {
        if (!$repositorio) {

            return '';
        }

        $delimiter = '|';
        if (preg_match('(\\:)', $get)) {
            $delimiter = ':';
        }

        $explodedGet = explode($delimiter, $get);
        $getcampo = $explodedGet[0];
        unset($explodedGet[0]);
        $shiftCampos = implode($delimiter, $explodedGet);

        $cmp = "get" . ucfirst($getcampo);
        if ($cmp == 'getIdiomas' && $idiomaPadrao) {
            $cmp = 'getIdiomaPadrao';
        }

        $getter = $repositorio->{$cmp}();

        if (count($explodedGet)) {
            $getter = self::get($getter, $shiftCampos);
        }

        return $getter;
    }

    public static function add($repositorio, $get, $data)
    {
        $delimiter = '|';
        if (preg_match('(\\:)', $get)) {
            $delimiter = ':';
        }

        $explodedGet = explode($delimiter, $get);
        $getcampo = $explodedGet[0];
        unset($explodedGet[0]);
        $shiftCampos = implode($delimiter, $explodedGet);

        $cmp = "add" . ucfirst($getcampo);
        $getter = $repositorio->{$cmp}($data);

        return $getter;
    }

    public static function remove($repositorio, $get)
    {
        $delimiter = '|';
        if (preg_match('(\\:)', $get)) {
            $delimiter = ':';
        }

        $explodedGet = explode($delimiter, $get);
        $getcampo = $explodedGet[0];
        unset($explodedGet[0]);
        $shiftCampos = implode($delimiter, $explodedGet);

        $cmp = "limpa" . ucfirst($getcampo);

        $repositorio->{$cmp}();
    }

    public static function set($repositorio, $set, $data)
    {
        $delimiter = '|';
        if (preg_match('(\\:)', $set)) {
            $delimiter = ':';
        }

        $explodedGet = explode($delimiter, $set);
        $getcampo = $explodedGet[0];
        unset($explodedGet[0]);
        $shiftCampos = implode($delimiter, $explodedGet);

        $cmp = "set" . ucfirst($getcampo);
        $getter = $repositorio->{$cmp}($data);

        return $getter;
    }

    public static function getFormContent($repositorio, $colunas)
    {
        $data = [];
        foreach ($colunas as $campo => $opcoes) {

            $busca = $campo;
            $delimiter = '|';
            if (preg_match('(\\:)', $campo)) {
                $delimiter = ':';
            }

            $campoEx = explode($delimiter, $campo);
            if ($opcoes['f-type'] == 'foreign') {

                $campoEx[1] = $opcoes['primary'];
                $busca = implode($delimiter, $campoEx);
            }

            if ($opcoes['f-type'] == 'many') {
                $data[$campo] = [];
                foreach (self::get($repositorio, $busca) as $target) {
                    $data[$campo][] = (int)self::get($target, $opcoes['primary']);
                }
            } elseif ($opcoes['f-type'] == 'idioma') {
                if (!(isset($opcoes['joinColumns']) && count($opcoes['joinColumns']))) {
                    $colsIdiomas = explode(':', $campo);
                    foreach (self::get($repositorio, $campoEx[0], false) as $idioma) {
                        $colunaArray = "{$colsIdiomas[0]}[" . self::get($idioma, 'idioma|id') . "][{$colsIdiomas[1]}]";
                        $data[$colunaArray] = (string)self::get($idioma, $campoEx[1]);
                    }
                } else {
                    $data[$campo] = (string)self::get(self::get($repositorio, $campoEx[0]), 'id');
                }
            } elseif ($opcoes['f-type'] == 'foreign') {
                $data[$campo] = (string)self::get(self::get($repositorio, $campoEx[0]), $opcoes['primary']);
            } else {
                if (self::get($repositorio, $campo) instanceof \DateTime)
                    $data[$campo] = (string)self::get($repositorio, $campo)->format('d-m-Y H:i:s');
                else
                    $data[$campo] = self::get($repositorio, $campo);
                if ($opcoes['type'] == 'boolean')
                    $data[$campo] = (int)self::get($repositorio, $campo);
            }
        }

        return $data;
    }

    /**
     * Gera o <b>rewrite</b> para determinado registro de uma tabela respeitando se há outros itens
     * com o mesmo nome nessa tabela, nesse caso é adicionada uma numeração (titulo, titulo-1, ...)
     *
     * @param \ZF3Abs\Repository\AbstractRepository $repository
     * @param bool $id
     * @param bool $value
     * @return String
     */
    public static function makeRewrite($repository, $id = false, $value = false)
    {

        $rewrite = $temp = Utils::urlAmigavel($value, '');

        $query = $repository->createQueryBuilder('a1')->select()->andWhere("a1.rewrite = '{$rewrite}'");
        if ($id && strlen($id) > 0) {
            $query->andWhere("a1.id <> {$id}");
        }

        $i = 1;
        while (count($query->getQuery()->execute()) > 0) {

            $rewrite = "$temp-$i";

            $query = $repository->createQueryBuilder('a1')->select()->andWhere("a1.rewrite = '{$rewrite}'");
            if ($id && strlen($id) > 0) {
                $query->andWhere("a1.id <> {$id}");
            }

            $i++;
        }

        return $rewrite;
    }

    /**
     * Retorna o cadastro do usuário, usando o email
     * @param $email
     * @return object|null
     */
    public static function usuario($email)
    {
        $em = Utils::getEntityManager();
        $mUser = $em->getRepository(\App\Entity\Usuario::class);

        return $mUser->findOneBy(['email' => $email]);
    }

    /**
     * Retorna a lista de idiomas
     * @return \App\Repository\IdiomaRepository|null
     */
    public static function getIdiomas()
    {
        if (!self::$_idiomas) {
            $mConf = Utils::getEntityManager()->getRepository(\App\Entity\Idioma::class);
            self::$_idiomas = $mConf->getListaOrdenada('padrao', 'DESC');
        }

        return self::$_idiomas;
    }

    /**
     * @param int $idInscricao
     * @param bool|\DateTime $data
     * @return mixed|InscricaoValor
     */
    public static function getValorInscricaoPorData(int $idInscricao, $data = false)
    {
        return Utils::getEntityManager()->getRepository(InscricaoValor::class)->buscaPorData($idInscricao, $data);
    }

    /**
     * @param $conf
     * @param null $newVal
     * @return bool
     */
    public static function Config($conf, $newVal = null)
    {
        if (!self::$_config) {
            self::$_config = Utils::getEntityManager()->getRepository(\App\Entity\Configs::class);
        }

        if (!is_null($newVal)) {
            self::ConfigSave($conf, $newVal);
        }

        /** @var Configs $config */
        $config = self::$_config->findOneBy(['nome' => $conf]);

        if ($config) {

            return $config->getValor();
        }

        return false;
    }

    /**
     * Se não existir a configuração vai trazer um valor padrão
     *
     * @param $conf
     * @param string $default
     * @return bool|string
     */
    public static function ConfigD($conf, $default = '')
    {
        if ($res = self::Config($conf)) {
            return $res;
        }

        return $default;
    }

    private static function ConfigSave($conf, $newVal)
    {
        /** @var Configs $config */
        $config = self::$_config->findOneBy(['nome' => $conf]);

        if ($config) {
            $config->setValor($newVal);
        } else {
            $config = new Configs();
            $config->setNome($conf);
            $config->setValor($newVal);
        }

        Utils::getEntityManager()->persist($config);
        Utils::getEntityManager()->flush();
    }
}