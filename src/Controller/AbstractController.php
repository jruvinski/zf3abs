<?php

namespace ZF3Abs\Controller;


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use ZF3Abs\Entity\AbstractEntity;
use ZF3Abs\Form\AbstractForm;
use ZF3Abs\Util\DB;

abstract class AbstractController extends AbstractActionController
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $_entityManager;

    /**
     * @var string
     */
    protected $_defaultRepository;

    /**
     * @var \ZF3Abs\Repository\AbstractRepository
     */
    protected $_defaultRepositoryLoaded;

    /**
     * @var \ZF3Abs\Entity\AbstractEntity
     */
    protected $_linhaRegistro;

    /**
     * @var array
     */
    protected $_colunasLista = [];

    /**
     * @var array
     */
    protected $_colunasForm = [];

    /**
     * @var array
     */
    protected $_colunasOpcoes = [];

    /**
     * @var array
     */
    protected $_customLayoutCampo = [];

    /**
     * Nome do controlador atual
     * @var string
     */
    protected $_controllerName = 'Abstract';

    /**
     * Configurações que substituem outras para determinadas colunas
     * @var array
     */
    protected $_colunasSuper = [];

    /**
     * Filtros que serão aplicados na busca da listagem
     * [['andWhere', 'id', '=', 1]]
     * @var array
     */
    protected $_filtros = [];

    /**
     * Botões auxiliares das listagens
     * Ex.: [['report', 'Ver relatório', 'fa-icon']]
     * @var array
     */
    protected $_customBtnAux = [];

    protected $_headerMessage = [];

    /**
     * Ações extras que podem ser feitas em massa nas listagens
     * ['ativar' => 'ativar']
     * @var array
     */
    protected $_acoesExtras = [];

    /**
     * Separação dos campos entre abas no formulário
     * @var array
     */
    protected $_tabs = [];

    /**
     * Força um campo a seguir um padrão, utiliza-se para selecionar entre select e TreeView
     * @var array
     */
    protected $_forcarTipoCampo = [];

    /**
     * @var \ZF3Abs\Form\AbstractForm
     */
    protected $_form;

    /**
     * @var null|int
     */
    protected $_nid = null;

    /**
     * Define se vai ter upload de imagens e qual a quantidade limite
     * @var int
     */
    protected $_imagens = 0;

    /**
     * Define se vai ter upload de arquivos e qual a quantidade limite
     * @var int
     */
    protected $_arquivos = 0;

    /**
     * Informações sobre cada coluna da tabela
     * @var array
     */
    protected $_infoColunas = [];

    /**
     * @var \Zend\Mvc\I18n\Translator
     */
    protected $_tr;

    /**
     * @var bool
     */
    protected $_redirecionarAposSalvar = true;

    /**
     * Rota padrão do módulo
     * @var string
     */
    protected $_defaultRoute = 'painel/painel';

    public function __construct($container)
    {
        $this->_entityManager = $container->get('doctrine.entitymanager.orm_default');
        $this->_defaultRepositoryLoaded = $this->_entityManager->getRepository($this->_defaultRepository);

        $this->_acoesExtras['ativar'] = 'Ativar';
        $this->_acoesExtras['inativar'] = 'Inativar';
        $this->_acoesExtras['excluir'] = 'Excluir';

        $this->_infoColunas = [
            '_colunasLista' => $this->_defaultRepositoryLoaded->infoColunas($this->_colunasLista, $this->_colunasOpcoes, $this->_colunasSuper),
            '_colunasForm' => $this->_defaultRepositoryLoaded->infoColunas($this->_colunasForm, $this->_colunasOpcoes, $this->_colunasSuper),
        ];
    }

    public function indexAction()
    {
        return (new ViewModel([
            'colunas' => $this->_infoColunas['_colunasLista'],
            'acoesExtras' => $this->_acoesExtras,
            'colunasOpcoes' => $this->_colunasOpcoes,
            'headerMessage' => $this->_headerMessage,
            'message' => [
                'success' => $this->flashMessenger()->getSuccessMessages(),
                'info' => $this->flashMessenger()->getInfoMessages(),
                'warning' => $this->flashMessenger()->getWarningMessages(),
                'danger' => $this->flashMessenger()->getErrorMessages(),
            ]
        ]))->setTemplate('painel/abstract/index');
    }

    /**
     * Retorna os resultados da busca realizada na tabela, esses dados são
     * devolvidos em formato JSON
     */
    public function listarAction()
    {
        $request = $this->getRequest();
        $query = $request->getQuery();
        $post = array_merge($this->params()->fromQuery(), $this->params()->fromPost());

        // Somente é possível acessar via ajax, ou caso queira baixar alguma planilha com os dados
        if ($request->isXmlHttpRequest() || isset($post['download'])) {

            $records = array();
            $columns = $this->_infoColunas['_colunasLista'];

            if ($request->isPost() || $request->isGet()) {

                // Usado para as ações em lote na tabela, ativar, desativar, excluir...
                if (isset($post["customActionType"]) && $post["customActionType"] == "group_action") {

                    $this->customAjax($post["customActionName"], isset($post['id']) ? $post['id'] : false);

                    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                    $records["customActionMessage"] = "Ação completada com sucesso!"; // pass custom message(useful for getting status of group actions)
                }

                // Monta a busca com os dados fornecidos
                $paramsBusca = isset($post['find']) ? $post['find'] : [];

                foreach ($paramsBusca as $k => $v) {

                    if ($v != "") {

                        if ($k != 'id') {

                            $type = $columns[$k]['type'];
                        } elseif ($k == 'id') {

                            $type = 'int';
                        }

                        $delimiter = '|';
                        if (preg_match('(\\:)', $k)) {
                            $delimiter = ':';
                        }

                        $exK = explode($delimiter, $k);
                        if (preg_match('/(date|datetime|timestamp)/', $type)) {

                            if (is_array($v) && ($v['from'] != "" && $v['to'] != "")) {

                                $data[0] = date("Y-m-d", strtotime($v['from']));
                                $data[1] = date("Y-m-d", strtotime($v['to']));
                                $inicial = $data[0] <= $data[1] ? $data[0] : $data[1];
                                $final = $data[0] <= $data[1] ? $data[1] : $data[0];

                                $this->_filtros[] = array('andWhere', "DATE({$k})", 'BEETWEEN', "'{$inicial}' AND '{$final}'");
                            } elseif (is_array($v) && ($v['from'] != "" || $v['to'] != "")) {

                                $v = date('Y-m-d', strtotime(($v['from'] == "" ? substr($v['to'], 0, 2) . "-" . substr($v['to'], 2, 2) . "-" . substr($v['to'], 4, 4) : substr($v['from'], 0, 2) . "-" . substr($v['from'], 2, 2) . "-" . substr($v['from'], 4, 4))));
                                $this->_filtros[] = array('andWhere', "DATE({$k})", '=', $v);
                            }
                        } elseif ($type == 'tinyint' || end($exK) == 'status') {

                            $this->_filtros[] = array('andWhere', $k, '=', $v);
                        } elseif (preg_match('/(int|integer|double|decimal)/', $type) && end($exK) != 'id') {

                            if (is_array($v) && ($v['from'] != "" && $v['to'] != "")) {

                                $this->_filtros[] = array('andWhere', $k, 'BETWEEN', "{$v['from']} AND {$v['to']}");
                            } elseif (is_array($v) && ($v['from'] != "" || $v['to'] != "")) {

                                $v = $v['from'] == "" ? $v['to'] : $v['from'];
                                $this->_filtros[] = array('andWhere', $k, '=', $v);
                            }
                        } elseif (preg_match('/(enum)/', $type) || end($exK) == 'id') {

                            $this->_filtros[] = array('andWhere', $k, '=', $v);
                        } elseif (preg_match('/(many)/', $columns[$k]['f-type'])) {
                            $this->_filtros[] = array('andWhere', "{$k}|id", '=', $v);
                        } else {

                            if (count($exK) == 3 && isset($this->_colunasOpcoes[$k]['subs-buscaalternativa'])) {
                                $this->_filtros[] = array('andWhere', $k, 'LIKE', "%{$v}%", $this->_colunasOpcoes[$k]['subs-buscaalternativa']);
                            } else {
                                $this->_filtros[] = array('andWhere', $k, 'LIKE', "%{$v}%");
                            }
                        }
                    }
                }

                $i = 0;
                foreach ($columns as $col => $options) {

                    $i++;
                    $order = $this->params()->fromPost('order');
                    if ($i == $order[0]['column']) {

                        if (strstr('|', $col) >= 0) {
                            $col = explode('|', $col)[0];
                        }
                        $this->_filtros[] = ['orderBy', $col, $order[0]['dir']];
                    }
                }

                foreach ($this->params()->fromPost('customSearch', []) as $act => $val) {

                    $this->_filtros[] = [$act, $val, ''];
                }
            }

            // Realiza a busca com os parametros setados acima
            $recordsDb = $this->_defaultRepositoryLoaded->buscaPainel($this->_filtros, $this->_infoColunas['_colunasLista']);
            // Se o controller precisar fazer algum ajuste depois de realizar a busca, usa esse método em seu controller
            $recordsDb = $this->alteracoesPosBusca($recordsDb);

            $sEcho = intval($this->params()->fromPost('draw'));
            $iTotalRecords = count($recordsDb);

            $records["data"] = $this->getRecordsInArray($recordsDb);

            // Manda para a geração de arquivo para download com os dados da busca
            if ($this->getRequest()->isPost() || $this->getRequest()->isGet()) {

                if ($download = $query->get('download', false)) {

                    $this->customAjax($download, $recordsDb, $this->_infoColunas['_colunasLista']);
                }
            }

            // Organiza e imprime os dados em JSON para que o javascript leia
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            $records["ajaxPost"] = http_build_query($this->params()->fromPost() + $this->params()->fromQuery());

            return new JsonModel($records);
        }

        $this->getResponse()->setStatusCode(404);
    }

    /**
     * @param $recordsDb
     * @return mixed
     */
    protected function alteracoesPosBusca($recordsDb)
    {
        return $recordsDb;
    }

    /**
     * @param $recordsDb
     * @return array
     * @throws \Exception
     */
    protected function getRecordsInArray($recordsDb)
    {
        $post = array_merge($this->params()->fromQuery(), $this->params()->fromPost());
        $iTotalRecords = count($recordsDb);
        $iDisplayLength = intval(isset($post['length']) ? $post['length'] : 0);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval(isset($post['start']) ? $post['start'] : 0);
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records = [];
        for ($i = $iDisplayStart; $i < $end; $i++) {

            $nidAtual = \ZF3Abs\Util\DB::get($recordsDb[$i], 'id');
            $records[$i][] = "<input type='checkbox' name='id[]' value='{$nidAtual}'>";

            foreach ($this->_infoColunas['_colunasLista'] as $k => $v) {

                $records[$i][] = $this->transformaColunasLista($recordsDb[$i], $k, $v);
            }

            $records[$i][] = $this->montarBotoesLista($recordsDb[$i], $nidAtual);
        }

        return $records;
    }

    /**
     * @param \Doctrine\ORM\PersistentCollection|\ZF3Abs\Repository\AbstractRepository $recDb
     * @param string $k Nome do campo a ser transformado
     * @param mixed $v Conteúdo do campo
     * @return array|\ZF3Abs\Repository\AbstractRepository|string
     * @throws \Exception
     */
    protected function transformaColunasLista($recDb, $k, $v)
    {

        $status = array(
            "3" => "danger|Apagado",
            "2" => "warning|Pendente",
            "1" => "success|Ativo",
            "0" => "danger|Inativo"
        );
        $tinyInt = array(
            "1" => "success|Sim",
            "0" => "danger|Não"
        );

        $delimiter = '|';
        if (preg_match('(\\:)', $k)) {
            $delimiter = ':';
        }

        $exK = explode($delimiter, $k);
        $fimExk = end($exK);
        $valorCampoBD = \ZF3Abs\Util\DB::get($recDb, $k);

        $rec = [];
        if (isset($this->_colunasOpcoes[$k]['subs-lista'])) {

            if (!$conteudo = \ZF3Abs\Util\DB::get($recDb, $this->_colunasOpcoes[$k]['subs-lista']))
                if (isset($this->_colunasOpcoes[$k]['subs-alternativa']))
                    $conteudo = \ZF3Abs\Util\DB::get($recDb, $this->_colunasOpcoes[$k]['subs-alternativa']);

            if (!$conteudo)
                $conteudo = '<span class="label label-default" title="Sem informações">Sem informações</span>';

            $rec = $conteudo;
        } elseif ($fimExk == 'status') {

            $exStatusV = explode('|', $status[$valorCampoBD]);
            $rec = "<span class='label label-sm change-status label-{$exStatusV[0]}'>{$exStatusV[1]}</span>";
        } elseif ($v['type'] == 'date') {

            $rec = $valorCampoBD->format('d-m-Y');
        } elseif ($v['type'] == 'datetime' || $v['type'] == 'timestamp') {

            $rec = $valorCampoBD->format('d-m-Y H:i:s');
        } elseif ($v['type'] == 'tinyint' || $v['type'] == 'boolean' || (isset($this->_colunasOpcoes[$fimExk]) && in_array('yes-no', $this->_colunasOpcoes[$fimExk]))) {

            $exStatusV = explode('|', $tinyInt[$valorCampoBD]);
            $rec = "<span class='label label-sm label-{$exStatusV[0]}'>{$exStatusV[1]}</span>";
        } elseif (end($exK) == 'arquivo' && preg_match('/(Arquivo)/', $this->_defaultRepository) && \ZF3Abs\Util\DB::get($recDb, 'tipo') == 'I') {

            $rec = "<img style='max-width: 40px;' src='" . \ZF3Abs\Util\File::getImageUrl(\ZF3Abs\Util\DB::get($recDb, 'id'), 'mini') . "'/>&nbsp{$valorCampoBD}";
        } else {

            $rec = $valorCampoBD;
        }

        return $rec;
    }

    /**
     * Cria um array de botões auxiliares para montagem html
     *
     * @param $recordsDb
     * @param $nidAtual
     * @return array
     */
    protected function getBotoesAuxiliares($recordsDb, $nidAtual)
    {
        $controller = $this->params()->fromRoute('controller', 'index');

        $auxiliares = [];
        foreach ($this->_customBtnAux as $k => $btn) {
            $action = $btn[0];
            $label = $btn[1];
            $icon = isset($btn[2]) ? $btn[2] : 'ellipsis-h';
            $position = isset($btn[3]) ? $btn[3] : 'prepend';
            if ($this->access("sistema.{$controller}.{$action}") && $position == 'prepend') {
                $params = array(
                    'controller' => $controller,
                    'action' => $action,
                    'nid' => $nidAtual
                );
                $auxiliares[] = "<a href='{$this->url()->fromRoute($this->_defaultRoute, $params)}'><i class='fa fa-{$icon}'></i> {$label}</a>";
            }
        }

        if ($this->access("sistema.{$controller}.atualizar")) {
            $params = array(
                'controller' => $controller,
                'action' => 'atualizar',
                'nid' => $nidAtual
            );
            $auxiliares[] = "<a href='{$this->url()->fromRoute($this->_defaultRoute, $params)}'><i class='fa fa-edit'></i> Editar</a>";
        }

        if ($this->access("sistema.{$controller}.ativar") && isset($columns['status']) && \ZF3Abs\Util\DB::get($recordsDb, 'status') == '0') {
            $params = array(
                'controller' => $controller,
                'action' => 'ativar',
                'nid' => $nidAtual
            );
            $auxiliares[] = "<a href='{$this->url()->fromRoute($this->_defaultRoute, $params)}'><i class='fa fa-check'></i> Ativar</a>";
        }

        if ($this->access("sistema.{$controller}.inativar") && isset($columns['status']) && \ZF3Abs\Util\DB::get($recordsDb, 'status') == '1') {
            $params = array(
                'controller' => $controller,
                'action' => 'inativar',
                'nid' => $nidAtual
            );
            $auxiliares[] = "<a href='{$this->url()->fromRoute($this->_defaultRoute, $params)}'><i class='fa fa-remove'></i> Inativar</a>";
        }

        if ($this->access("sistema.{$controller}.excluir")) {
            $params = array(
                'controller' => $controller,
                'action' => 'excluir',
                'nid' => $nidAtual
            );
            $auxiliares[] = "<a href=\"{$this->url()->fromRoute($this->_defaultRoute, $params)}\" "
                . ' class="extra-btn-delete"><i class="fa fa-trash"></i> Excluir</a>';
        }

        foreach ($this->_customBtnAux as $k => $btn) {
            $action = $btn[0];
            $label = $btn[1];
            $icon = isset($btn[2]) ? $btn[2] : 'ellipsis-h';
            $position = isset($btn[3]) ? $btn[3] : 'prepend';
            if ($this->access("sistema.{$controller}.{$action}") && $position == 'append') {
                $params = array(
                    'controller' => $controller,
                    'action' => $action,
                    'nid' => $nidAtual
                );
                $auxiliares[] = "<a href='{$this->url()->fromRoute($this->_defaultRoute, $params)}'><i class='fa fa-{$icon}'></i> {$label}</a>";
            }
        }

        if (count($auxiliares)) {
            $auxiliares[0] = str_replace('href=', " class='btn btn-sm btn-default' href=", $auxiliares[0]);
        }

        return $auxiliares;
    }

    /**
     * Monta a estrutura dos botões da listagem, com os os botões fornecidos
     * @param $recordsDb
     * @param $nidAtual
     * @return string
     */
    protected function montarBotoesLista($recordsDb, $nidAtual)
    {
        $auxiliares = $this->getBotoesAuxiliares($recordsDb, $nidAtual);

        $btnAuxiliar = '';
        if (count($auxiliares)) {
            $firstAux = $auxiliares[0];
            unset($auxiliares[0]);
            $btnAuxiliar = '<div class="btn-group pull-right">' . $firstAux;
            if (count($auxiliares)) {
                $btnAuxiliar .= '
                            <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu " role="menu">
                                <li>' . implode('</li><li>', $auxiliares) . '</li>
                            </ul>';
            }
            $btnAuxiliar .= '</div>';
        }

        return $btnAuxiliar;
    }

    /**
     * Executa as ações em massa que existem na lista de registros (ativa, desativa, exclui)
     *
     * @param $action
     * @param $params
     * @param bool $columns
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    protected function customAjax($action, $params, $columns = false)
    {

        switch ($action) {
            // Ativar registros da tabela
            case "ativar":

                foreach ($params as $field) {

                    $this->_defaultRepositoryLoaded->salvaRepo(['status' => AbstractEntity::STATUS_ATIVO, 'id' => $field], $this->_infoColunas['_colunasForm'], $this->_defaultRepository);
                }

                break;
            // Desativar registros da tabela
            case "inativar":

                foreach ($params as $field) {

                    $this->_defaultRepositoryLoaded->salvaRepo(['status' => AbstractEntity::STATUS_INATIVO, 'id' => $field], $this->_infoColunas['_colunasForm'], $this->_defaultRepository);
                }

                break;
            // "Apagar" registros da tabela, por segurança não apagamos, apenas marcamos como apagado
            case "delete":

                foreach ($params as $field) {

                    $this->_defaultRepositoryLoaded->salvaRepo(['status' => AbstractEntity::STATUS_APAGADO, 'id' => $field], $this->_infoColunas['_colunasForm'], $this->_defaultRepository);
                }

                break;
            // Salva os itens da lista em Excel e disponibiliza para download
            case "excel":

                $status = array(
                    "3" => "danger|Apagado",
                    "2" => "warning|Pendente",
                    "1" => "success|Ativo",
                    "0" => "danger|Inativo"
                );
                $tinyInt = array(
                    "1" => "success|Sim",
                    "0" => "danger|Não"
                );

                $fileName = $this->params()->fromRoute('controller', 'index') . "-lista";


                $data = [];
                $titulos = [];

                foreach ($columns as $opt) {
                    $titulos[] = $opt['label'];
                }
                $data[] = $titulos;

                foreach ($params as $val) {
                    $dt = [];
                    foreach ($columns as $col => $opt) {
                        if ($col == 'pagamento|status|nome') {
                            if ($pg = DB::get($val, $col)) {
                                $dt[] = $pg;
                            } else {
                                $dt[] = DB::get($val, 'empenho|status|nome');
                            }
                        } elseif ($col == 'status') {
                            $valorCampoBD = DB::get($val, $col);
                            $exStatusV = explode('|', $status[$valorCampoBD]);
                            $dt[] = $exStatusV[1];
                        } else {
                            $dt[] = DB::get($val, $col);
                        }
                    }
                    $data[] = $dt;
                }

                $spreadsheet = new Spreadsheet();
                $spreadsheet->removeSheetByIndex(0);

                $sheetDados = new Worksheet($spreadsheet, 'Dados');

                for ($i = 'A'; $i < 'Z'; $i++) {
                    $sheetDados->getColumnDimension($i)->setAutoSize(true);
                }

                $sheetDados->fromArray($data);

                $spreadsheet->addSheet($sheetDados);

                for ($i = 'A'; $i < 'Z'; $i++) {
                    $spreadsheet->getActiveSheet()->getStyle("{$i}1")->getFont()->setBold(true);
                }

                header('Content-Type: application/vnd.ms-excel');
                header("Content-Disposition: attachment;filename={$fileName}.xls");
                header('Cache-Control: max-age=0');
                header('Cache-Control: max-age=1');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');

                $writer = IOFactory::createWriter($spreadsheet, 'Xls');
                $writer->save('php://output');
                exit;

                break;
            default:

                foreach ($params as $nid) {
                    $this->_defaultRepositoryLoaded->$action($nid);
                }
                break;
        }

        return;
    }

    public function adicionarAction()
    {
        $opt = [
            'imagem' => $this->_imagens,
            'arquivo' => $this->_arquivos,
            'opcoes' => $this->_colunasOpcoes
        ];
        $this->_form = new AbstractForm($this->_entityManager, $this->_defaultRepository, $this->_infoColunas['_colunasForm'], $opt, $this->getRequest()->isXmlHttpRequest());
        $this->_form->setAttribute('action', $this->url()->fromRoute($this->_defaultRoute, ['controller' => $this->params()->fromRoute('controller', 'index'), 'action' => 'salvar']));

        // Se for ajax, não usa as tabs
//        if ($this->getRequest()->isXmlHttpRequest())
//            $this->_tabs = [];

        return (new ViewModel([
            'tabs' => $this->_tabs,
            'form' => $this->_form,
            'isAjax' => $this->getRequest()->isXmlHttpRequest(),
            'nimagem' => $this->_imagens,
            'narquivo' => $this->_arquivos,
            'superCampo' => $this->_colunasSuper,
            'customLayoutCampo' => $this->_customLayoutCampo,
            'headerMessage' => $this->_headerMessage,
            'message' => [
                'success' => $this->flashMessenger()->getSuccessMessages(),
                'info' => $this->flashMessenger()->getInfoMessages(),
                'warning' => $this->flashMessenger()->getWarningMessages(),
                'danger' => $this->flashMessenger()->getErrorMessages(),
            ]
        ]))->setTemplate('painel/abstract/form')->setTerminal($this->getRequest()->isXmlHttpRequest());
    }

    public function atualizarAction()
    {
        $this->_nid = $this->_nid ?? (int)$this->params()->fromRoute('nid', -1);

        if ($this->_nid < 1 || !$row = $this->_defaultRepositoryLoaded->buscaId($this->_nid)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $this->_linhaRegistro = $row;

        $opt = [
            'imagem' => $this->_imagens,
            'arquivo' => $this->_arquivos,
            'opcoes' => $this->_colunasOpcoes
        ];
        $this->_form = new AbstractForm($this->_entityManager, $this->_defaultRepository, $this->_infoColunas['_colunasForm'], $opt, $this->getRequest()->isXmlHttpRequest());
        $this->_form->setAttribute('action', $this->url()->fromRoute($this->_defaultRoute, ['controller' => $this->params()->fromRoute('controller', 'index'), 'action' => 'salvar']));

        $dadosForm = DB::getFormContent($row, $this->_infoColunas['_colunasForm']);

        // Faz a busca do item padrão do select2 configurado como ajax
        foreach ($this->_form->getElements() as $el) {
            $elName = str_replace(['[', ']'], '', $el->getName());
            if (isset($this->_colunasOpcoes[$elName]) && in_array('one-select2', $this->_colunasOpcoes[$elName], true)) {
                $val = $dadosForm[$elName];
                $repo = $this->_entityManager->getRepository($this->_infoColunas['_colunasForm'][$elName]['target']);
                $row = $repo->findOneBy([$this->_infoColunas['_colunasForm'][$elName]['primary'] => $val]);

                $campos = in_array('one-select2', $this->_colunasOpcoes[$elName], true) ?
                    explode('-', $this->_colunasOpcoes[$elName]['campos']) :
                    @end(explode('-', $elName));

                $text = '';
                foreach ($campos as $k => $campo) {
                    $text .= DB::get($row, $campo);
                    if (count($campos) > ($k + 1))
                        $text .= ' / ';
                }
                $el->setValueOptions([DB::get($row, $this->_infoColunas['_colunasForm'][$elName]['primary']) => $text]);
            }

            if (isset($this->_colunasOpcoes[$elName]) && in_array('many-select2', $this->_colunasOpcoes[$elName], true)) {
                $val = $dadosForm[$elName];
                $repo = $this->_entityManager->getRepository($this->_infoColunas['_colunasForm'][$elName]['target']);
                $rows = $repo->findBy([$this->_infoColunas['_colunasForm'][$elName]['primary'] => $val]);

                $opc = [];
                $campos = in_array('many-select2', $this->_colunasOpcoes[$elName], true) ?
                    explode('-', $this->_colunasOpcoes[$elName]['campos']) :
                    @end(explode('-', $elName));

                foreach ($rows as $row) {
                    $text = '';
                    foreach ($campos as $k => $campo) {
                        $text .= DB::get($row, $campo);
                        if (count($campos) > ($k + 1))
                            $text .= ' / ';
                    }
                    $opc[DB::get($row, $this->_infoColunas['_colunasForm'][$elName]['primary'])] = $text;
                }
                $el->setValueOptions($opc);
            }
        }

        $this->_form->setData($dadosForm);

        return (new ViewModel([
            'tabs' => $this->_tabs,
            'form' => $this->_form,
            'isAjax' => $this->getRequest()->isXmlHttpRequest(),
            'nimagem' => $this->_imagens,
            'narquivo' => $this->_arquivos,
            'superCampo' => $this->_colunasSuper,
            'customLayoutCampo' => $this->_customLayoutCampo,
            'headerMessage' => $this->_headerMessage,
            'message' => [
                'success' => $this->flashMessenger()->getSuccessMessages(),
                'info' => $this->flashMessenger()->getInfoMessages(),
                'warning' => $this->flashMessenger()->getWarningMessages(),
                'danger' => $this->flashMessenger()->getErrorMessages(),
            ]
        ]))->setTemplate('painel/abstract/form');
    }

    public function salvarAction()
    {
        $colunas = $this->_infoColunas['_colunasForm'];
        $controller = $this->params()->fromRoute('controller', 'index');

        $opt = [
            'imagem' => $this->_imagens,
            'arquivo' => $this->_arquivos,
            'opcoes' => $this->_colunasOpcoes
        ];
        $this->_form = new AbstractForm($this->_entityManager, $this->_defaultRepository, $colunas, $opt);

        $data = $this->params()->fromPost();
        foreach ($data as $col => &$vals) {
            if (isset($colunas[$col]['f-type']) && $colunas[$col]['f-type'] == 'many' && is_array($vals)) {
                foreach ($vals as $id => $val) {
                    if (!is_numeric($val)) {
                        unset($vals[$id]);
                    }
                }
            }
            if (isset($colunas[$col]['f-type']) && $colunas[$col]['f-type'] == 'foreign' && ($vals == '' || $vals == '0')) {
                $vals = null;
            }
        }

        if (isset($data['file_ids'])) {
            unset($data['file_ids'], $data['file_desc']);
        }

        $this->_form->setData($data);

//        echo '<pre>';
//        var_dump($data, $this->_form->isValid(), $this->_form->getMessages(), $this->_form->getElements()['instituicao']);
//        exit;
        if ($this->getRequest()->isPost() && $this->_form->isValid()) {

            $this->_linhaRegistro = $this->_defaultRepositoryLoaded->salvaRepo($data, $this->_infoColunas['_colunasForm'], $this->_defaultRepository);
            $this->_nid = $this->_linhaRegistro->getId();

            $this->flashMessenger()->addSuccessMessage(
                'Registro ' .
                (isset($this->_form->getData()['id']) && $this->_form->getData()['id'] == '' ? 'adicionado' : 'atualizado') .
                ' com sucesso!'
            );
            if (count($this->params()->fromPost('file_ids', []))) {
                $mArquivo = $this->_entityManager->getRepository(\App\Entity\Arquivo::class);
                $mControlador = $this->_entityManager->getRepository(\App\Entity\Controlador::class);
                $mArquivoControlador = $this->_entityManager->getRepository(\App\Entity\ControladorArquivo::class);
                $controller_id = $mControlador->findOneBy(['nome' => $controller])->getId();
                $mArquivoControlador->removeArquivosController($controller_id, $this->_nid);
                $descs = $this->params()->fromPost('file_desc', []);
                $mostrar = $this->params()->fromPost('file_mostrar', []);
                foreach ($this->params()->fromPost('file_ids', []) as $k => $arq) {
                    $dados = [
                        'id' => $arq,
                        'descricao' => $descs[$k],
                        'capa' => (boolean)($arq == $this->params()->fromPost('file_capa', false)),
                        'mostrar' => isset($mostrar[$k]) && $mostrar[$k] == '1'
                    ];
                    $cols = $mArquivo->infoColunas(['id', 'descricao', 'capa', 'mostrar']);
                    $mArquivo->salvaRepo($dados, $cols, \App\Entity\Arquivo::class);

                    $dados2 = [
                        'id' => false,
                        'controlador|nome' => $controller_id,
                        'arquivo|arquivo' => $arq,
                        'registro' => $this->_nid,
                        'ordem' => $k
                    ];
                    $cols2 = $mArquivoControlador->infoColunas(['id', 'controlador|nome', 'arquivo|arquivo', 'registro', 'ordem']);
                    $mArquivoControlador->salvaRepo($dados2, $cols2, \App\Entity\ControladorArquivo::class);
                }
            }

            if ($this->getRequest()->isXmlHttpRequest()) {

                $retorno = [];
                $buscaPainel = $this->_defaultRepositoryLoaded->buscaPainel([['orderBy', 'nome', 'ASC']]);
                foreach ($buscaPainel as $linha) {
                    $retorno[] = [
                        'id' => $linha->getId(),
                        'active' => $linha->getNome() == $data['nome'],
                        'val' => $linha->getNome()
                    ];
                }
                echo json_encode($retorno);
                exit;
            }

            if ($this->_redirecionarAposSalvar)
                return $this->redirect()->toRoute($this->_defaultRoute, ['controller' => $controller]);
            else
                return;
        }

        $erros = [];
        foreach ($this->_form->getMessages() as $campo => $msgs) {
            $erros[] = $campo . ' - ' . implode(' / ', $msgs);
        }

        $this->flashMessenger()->addErrorMessage('Não foi possível adicionar o registro, os seguintes erros foram encontrados:<br>* ' . implode('<br>* ', $erros));
        return $this->redirect()->toRoute($this->_defaultRoute, ['controller' => $controller]);
    }

    public function ativarAction()
    {

        if ($nid = $this->params()->fromRoute('nid', false)) {
            $this->_defaultRepositoryLoaded->ativar($nid);
        }

        $controller = $this->params()->fromRoute('controller', 'index');
        return $this->redirect()->toRoute($this->_defaultRoute, ['controller' => $controller]);
    }

    public function inativarAction()
    {

        if ($nid = $this->params()->fromRoute('nid', false)) {
            $this->_defaultRepositoryLoaded->inativar($nid);
        }

        $controller = $this->params()->fromRoute('controller', 'index');
        return $this->redirect()->toRoute($this->_defaultRoute, ['controller' => $controller]);
    }

    public function excluirAction()
    {

        if ($nid = $this->params()->fromRoute('nid', false)) {
            $this->_defaultRepositoryLoaded->excluir($nid);
        }

        $controller = $this->params()->fromRoute('controller', 'index');
        return $this->redirect()->toRoute($this->_defaultRoute, ['controller' => $controller]);
    }
}